# A Splendid Little War #

Shawn Cowles, 2016

A 2D space RTS, runs as a seperate browser-based client and a server. The server can run on both windows and linux (via mono-service), while the client can be served as a static web-page.

--------------------------------------------

Code and assets are licensed under the MIT License. See LICENSE.txt for details.
Assets from third party sources are listed under "CREDITS.txt".