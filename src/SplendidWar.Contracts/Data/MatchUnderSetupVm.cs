﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Data
{
    [TsClass(Module = "", Name = "IMatchUnderSetupVm")]
    [DataContract]
    public class MatchUnderSetupVm
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string HostId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public PlayerSlot[] Slots { get; set; }
    }
}
