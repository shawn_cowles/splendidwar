﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Data
{
    [TsClass(Module = "", Name = "IGameObjectUpdate")]
    [DataContract]
    public class GameObjectUpdate
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public bool RemoveObject { get; set; }

        [DataMember]
        public Position Position { get; set; }

        [DataMember]
        public double Facing { get; set; }

        [DataMember]
        public ComponentUpdate[] ComponentUpdates { get; set; }

        [DataMember]
        public double Timestamp { get; set; }

        public GameObjectUpdate(int objectId)
        {
            Id = objectId;
            ComponentUpdates = new ComponentUpdate[0];
        }

        public static GameObjectUpdate RemoveUpdate(int objectId)
        {
            return new GameObjectUpdate(objectId)
            {
                RemoveObject = true
            };
        }
    }
}
