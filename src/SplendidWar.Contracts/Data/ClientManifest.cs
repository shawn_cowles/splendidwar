﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Data
{
    [TsClass(Module = "", Name = "IClientManifest")]
    [DataContract]
    public class ClientManifest
    {
        [DataMember]
        public string[] ShipImages { get; set; }
    }
}
