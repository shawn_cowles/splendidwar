﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Data
{
    [TsClass(Module = "", Name = "IPosition")]
    [DataContract]
    public class Position
    {
        [DataMember]
        public double X { get; set; }

        [DataMember]
        public double Y { get; set; }
    }
}