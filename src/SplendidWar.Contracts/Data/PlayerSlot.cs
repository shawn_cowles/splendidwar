﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Data
{
    [TsClass(Module = "", Name = "IPlayerSlot")]
    [DataContract]
    public class PlayerSlot
    {
        [DataMember]
        public string PlayerName { get; private set; }

        [DataMember]
        public string PlayerId { get; private set; }

        [DataMember]
        public bool HasBot { get; private set; }

        [DataMember]
        public bool HasPlayer
        {
            get { return PlayerId != null; }
        }

        public void ClearSlot()
        {
            PlayerId = null;
            HasBot = false;
        }

        public void SetPlayer(string playerId, string playerName)
        {
            PlayerId = playerId;
            PlayerName = playerName;
            HasBot = false;
        }

        public void SetBot()
        {
            HasBot = true;
        }
    }
}
