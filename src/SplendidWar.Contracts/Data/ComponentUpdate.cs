﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Data
{
    [TsClass(Module = "", Name = "IComponentUpdate")]
    [DataContract]
    public class ComponentUpdate
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ComponentName { get; set; }

        [DataMember]
        public bool RemoveComponent { get; set; }

        [DataMember]
        public List<object> Data { get; set; }

        public ComponentUpdate(int componentId, string spriteName)
        {
            Id = componentId;
            ComponentName = spriteName;
            Data = new List<object>();
        }

        public static ComponentUpdate BuildRemoveUpdate(int componentId)
        {
            return new ComponentUpdate(componentId, "")
            {
                RemoveComponent = true
            };
        }
    }
}
