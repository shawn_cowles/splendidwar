﻿using TypeLite;

namespace SplendidWar.Contracts.Messages
{
    [TsClass(Module = "", Name = "IStartQuickMatchRequest")]
    public class StartQuickMatchRequest : AbstractIncomingClientMessage
    {
        public const string NAME = "StartQuickMatchRequest";
        
        public StartQuickMatchRequest()
            : base(NAME)
        {
        }
    }
}
