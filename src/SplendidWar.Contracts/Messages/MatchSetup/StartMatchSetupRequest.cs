﻿using TypeLite;

namespace SplendidWar.Contracts.Messages.MatchSetup
{
    [TsClass(Module = "", Name = "IStartMatchSetupRequest")]
    public class StartMatchSetupRequest : AbstractIncomingClientMessage
    {
        public const string NAME = "StartMatchSetupRequest";
        
        public StartMatchSetupRequest()
            : base(NAME)
        {
        }
    }
}
