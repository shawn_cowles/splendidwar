﻿using TypeLite;

namespace SplendidWar.Contracts.Messages.MatchSetup
{
    [TsClass(Module = "", Name = "IMatchListRequest")]
    public class MatchListRequest : AbstractIncomingClientMessage
    {
        public const string NAME = "MatchListRequest";
        
        public MatchListRequest()
            : base(NAME)
        {
        }
    }
}
