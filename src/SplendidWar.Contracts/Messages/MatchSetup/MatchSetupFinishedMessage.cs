﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Messages.MatchSetup
{
    [TsClass(Module = "", Name = "IMatchSetupFinishedMessage")]
    [DataContract]
    public class MatchSetupFinishedMessage : AbstractIncomingClientMessage
    {
        public const string NAME = "MatchSetupFinishedMessage";
        
        [DataMember]
        public string MatchId { get; set; }

        public MatchSetupFinishedMessage()
            : base(NAME)
        {
        }
    }
}
