﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Messages.MatchSetup
{
    [TsClass(Module = "", Name = "IKickedFromMatchMessage")]
    [DataContract]
    public class KickedFromMatchMessage : AbstractOutgoingClientMessage
    {
        public const string NAME = "KickedFromMatchMessage";
        
        public KickedFromMatchMessage(string[] clientIds)
            : base(NAME, clientIds)
        {
        }

        public KickedFromMatchMessage(string clientId)
            : base(NAME, clientId)
        {
        }
    }
}
