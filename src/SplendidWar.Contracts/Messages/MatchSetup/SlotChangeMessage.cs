﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Messages.MatchSetup
{
    [TsClass(Module = "", Name = "ISlotChangeMessage")]
    [DataContract]
    public class SlotChangeMessage : AbstractIncomingClientMessage
    {
        public const string NAME = "SlotChangeMessage";

        [DataMember]
        public string MatchId { get; set; }

        [DataMember]
        public int SlotIndex { get; set; }

        [DataMember]
        public bool ClearSlot { get; set; }

        [DataMember]
        public bool SetBot { get; set; }

        public SlotChangeMessage()
            : base(NAME)
        {
        }
    }
}
