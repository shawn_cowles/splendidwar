﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Messages.MatchSetup
{
    [TsClass(Module = "", Name = "IJoinMatchRequest")]
    [DataContract]
    public class JoinMatchRequest : AbstractIncomingClientMessage
    {
        public const string NAME = "JoinMatchRequest";

        [DataMember]
        public string MatchId { get; set; }

        public JoinMatchRequest()
            : base(NAME)
        {
        }
    }
}
