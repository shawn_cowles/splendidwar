﻿using TypeLite;

namespace SplendidWar.Contracts.Messages.MatchSetup
{
    [TsClass(Module = "", Name = "ILeaveMatchSetupMessage")]
    public class LeaveMatchSetupMessage : AbstractIncomingClientMessage
    {
        public const string NAME = "LeaveMatchSetupMessage";
        
        public LeaveMatchSetupMessage()
            : base(NAME)
        {
        }
    }
}
