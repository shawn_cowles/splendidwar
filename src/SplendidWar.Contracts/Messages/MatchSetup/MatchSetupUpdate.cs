﻿using System.Runtime.Serialization;
using SplendidWar.Contracts.Data;
using TypeLite;

namespace SplendidWar.Contracts.Messages.MatchSetup
{
    [TsClass(Module = "", Name = "IMatchSetupUpdate")]
    [DataContract]
    public class MatchSetupUpdate : AbstractOutgoingClientMessage
    {
        public const string NAME = "MatchSetupUpdate";
        
        [DataMember]
        public MatchUnderSetupVm Match { get; set; }
        
        public MatchSetupUpdate(string[] clientIds, MatchUnderSetupVm match)
            : base(NAME, clientIds)
        {
            Match = match;
        }
    }
}
