﻿using System.Runtime.Serialization;
using SplendidWar.Contracts.Data;
using TypeLite;

namespace SplendidWar.Contracts.Messages.MatchSetup
{
    [TsClass(Module = "", Name = "IMatchListReply")]
    [DataContract]
    public class MatchListReply : AbstractOutgoingClientMessage
    {
        public const string NAME = "MatchListReply";
        
        [DataMember]
        public MatchUnderSetupVm[] Matches { get; set; }
        
        public MatchListReply(string clientId)
            : base(NAME, clientId)
        {
        }
    }
}
