﻿namespace SplendidWar.Contracts.Messages
{
    public class UserDisconnectedMessage : AbstractIncomingClientMessage
    {
        public const string NAME = "UserDisconnectedMessage";
        
        public UserDisconnectedMessage(string sessionId)
            : base(NAME)
        {
            ClientId = sessionId;
        }
    }
}
