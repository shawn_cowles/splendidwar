﻿namespace SplendidWar.Contracts.Messages
{
    public abstract class PlayerInputMessage : AbstractIncomingClientMessage
    {
        protected PlayerInputMessage(string messageName) 
            : base(messageName)
        {
        }
    }
}
