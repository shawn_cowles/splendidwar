﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Messages
{
    [TsClass(Module = "", Name = "IAbstractClientMessage")]
    [DataContract]
    public abstract class AbstractClientMessage : AbstractMessage
    {
        [DataMember]
        public string MessageName { get; private set; }
        
        protected AbstractClientMessage(string messageName)
        {
            MessageName = messageName;
        }
    }
}
