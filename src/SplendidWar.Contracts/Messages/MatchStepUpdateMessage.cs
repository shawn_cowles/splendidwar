﻿using System.Runtime.Serialization;
using SplendidWar.Contracts.Data;
using TypeLite;

namespace SplendidWar.Contracts.Messages
{
    [TsClass(Module = "", Name = "IMatchStepUpdateMessage")]
    [DataContract]
    public class MatchStepUpdateMessage : AbstractOutgoingClientMessage
    {
        public const string NAME = "MatchStepUpdateMessage";
        
        [DataMember]
        public GameObjectUpdate[] Updates { get; set; }

        public MatchStepUpdateMessage(string[] clientsToUpdate, GameObjectUpdate[] updates)
            :base(NAME, clientsToUpdate)
        {
            Updates = updates;
        }
    }
}
