﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Messages
{
    [TsClass(Module = "", Name = "IMatchStartingMessage")]
    [DataContract]
    public class MatchStartingMessage : AbstractOutgoingClientMessage
    {
        public const string NAME = "MatchStartingMessage";
        
        [DataMember]
        public string MatchId { get; set; }

        [DataMember]
        public int MapWidth { get; set; }

        [DataMember]
        public int MapHeight { get; set; }
        
        public MatchStartingMessage(string[] clientIds, string matchId, int mapWidth, int mapHeight)
            : base(NAME, clientIds)
        {
            MatchId = matchId;
            MapWidth = mapWidth;
            MapHeight = mapHeight;
        }
    }
}
