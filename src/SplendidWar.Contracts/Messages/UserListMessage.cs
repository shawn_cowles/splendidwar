﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Messages
{
    [TsClass(Module = "", Name = "IUserListMessage")]
    [DataContract]
    public class UserListMessage : AbstractOutgoingClientMessage
    {
        public const string NAME = "UserListMessage";

        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public string[] Usernames { get; set; }
        
        public UserListMessage(string clientId)
            : base(NAME, clientId)
        {
        }
    }
}
