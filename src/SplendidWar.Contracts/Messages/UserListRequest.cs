﻿using TypeLite;

namespace SplendidWar.Contracts.Messages
{
    [TsClass(Module = "", Name = "IUserListRequest")]
    public class UserListRequest : AbstractIncomingClientMessage
    {
        public const string NAME = "UserListRequest";
        
        public UserListRequest()
            : base(NAME)
        {
        }
    }
}
