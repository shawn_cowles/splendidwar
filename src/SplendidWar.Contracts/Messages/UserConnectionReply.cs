﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Messages
{
    [TsClass(Module = "", Name = "IUserConnectionReply")]
    [DataContract]
    public class UserConnectionReply : AbstractOutgoingClientMessage
    {
        public const string NAME = "UserConnectionReply";
        
        [DataMember]
        public string ClientId { get; set; }
        
        public UserConnectionReply(string clientId)
            : base(NAME, clientId)
        {
            ClientId = clientId;
        }
    }
}
