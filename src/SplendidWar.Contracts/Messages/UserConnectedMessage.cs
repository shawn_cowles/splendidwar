﻿using System.Runtime.Serialization;
using TypeLite;

namespace SplendidWar.Contracts.Messages
{
    [TsClass(Module = "", Name = "IUserConnectedMessage")]
    [DataContract]
    public class UserConnectedMessage : AbstractIncomingClientMessage
    {
        public const string NAME = "UserConnectedMessage";
        
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Username { get; set; }
        
        public UserConnectedMessage()
            : base(NAME)
        {
        }
    }
}
