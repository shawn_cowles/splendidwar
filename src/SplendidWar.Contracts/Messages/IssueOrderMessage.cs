﻿using System.Runtime.Serialization;
using SplendidWar.Contracts.Data;
using TypeLite;

namespace SplendidWar.Contracts.Messages
{
    [TsClass(Module = "", Name = "IIssueOrderMessage")]
    [DataContract]
    public class IssueOrderMessage : PlayerInputMessage
    {
        public const string NAME = "IssueOrderMessage";

        [DataMember]
        public int[] OrderedObjectIds { get; set; }

        [DataMember]
        public OrderType Order { get; set; }

        [DataMember]
        public int[] TargetIds { get; set; }

        [DataMember]
        public double TargetPositionX { get; set; }

        [DataMember]
        public double TargetPositionY { get; set; }

        public IssueOrderMessage()
            : base(NAME)
        {
        }
    }
}
