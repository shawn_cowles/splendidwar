﻿/// <reference path="AbstractComponent.ts" />

class OrderHandler extends AbstractComponent
{
    public CurrentOrder: string;
    
    constructor(update: IComponentUpdate, parent: GameObject)
    {
        super(parent);

        this.CurrentOrder = <string>update.Data[0];
    }
    
    protected Initialize(state: AbstractGameState): void
    {
    }

    protected Step(state: AbstractGameState, elapsedSeconds: number): void
    {
    }

    public LateStep(state: AbstractGameState, elapsedSeconds: number): void
    {
    }
    
    public Destroy(): void
    {
    }

    public ApplyUpdate(update: IComponentUpdate): void
    {
        this.CurrentOrder = <string>update.Data[0];
    }
}