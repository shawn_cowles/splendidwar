﻿/// <reference path="AbstractComponent.ts" />

class Selectable extends AbstractComponent
{
    public ObjectName: string;

    public SelectedByPointer: boolean;
    
    constructor(update: IComponentUpdate, parent: GameObject)
    {
        super(parent);

        this.ObjectName = update.Data[0];
    }
    
    protected Initialize(state: AbstractGameState): void
    {
    }

    protected Step(state: AbstractGameState, elapsedSeconds: number): void
    {
    }

    public LateStep(state: AbstractGameState, elapsedSeconds: number): void
    {
        // reset for next frame
        this.SelectedByPointer = false;
    }
    
    public Destroy(): void
    {
    }

    public ApplyUpdate(update: IComponentUpdate): void
    {
    }
}