﻿abstract class AbstractComponent
{
    public Id: number;
    public Parent: GameObject;
    public IsRemoved: boolean;

    private isInitialized: boolean;

    constructor(parent: GameObject)
    {
        this.Parent = parent;
        this.isInitialized = false;
        this.IsRemoved = false;
    }
    
    protected abstract Initialize(state: AbstractGameState): void;
    
    public DoStep(state: AbstractGameState, elapsedSeconds: number): void
    {
        if (!this.isInitialized)
        {
            this.Initialize(state);

            this.isInitialized = true;
        }

        this.Step(state, elapsedSeconds);
    }

    protected abstract Step(state: AbstractGameState, elapsedSeconds: number): void;
    
    public abstract LateStep(state: AbstractGameState, elapsedSeconds: number): void;
    
    public abstract Destroy(): void;

    public abstract ApplyUpdate(update: IComponentUpdate): void;

    public static Create(update: IComponentUpdate, parent: GameObject): AbstractComponent
    {
        switch (update.ComponentName)
        {
            case "SpriteDisplay":
                return new SpriteDisplay(update, parent);
            case "Ownership":
                return new Ownership(update, parent);
            case "Selectable":
                return new Selectable(update, parent);
            case "OrderHandler":
                return new OrderHandler(update, parent);
            default:
                throw "Unknown component name: " + update.ComponentName;
        }
    }
}