﻿/// <reference path="AbstractComponent.ts" />

class SpriteDisplay extends AbstractComponent
{
    private sprite: Phaser.Sprite;
    private update: IComponentUpdate;
    private selectable: Selectable;

    constructor(update: IComponentUpdate, parent: GameObject)
    {
        super(parent);

        this.update = update;
    }
    
    protected Initialize(state: AbstractGameState): void
    {
        this.sprite = state.SpriteGroups[<number>this.update.Data[3]]
            .create(0, 0, <string>this.update.Data[0]);
        this.sprite.anchor.set(0.5, 0.5);
        this.sprite.tint = <number>this.update.Data[1];
        this.sprite.scale = new Phaser.Point(
            <number>this.update.Data[2],
            <number>this.update.Data[2]);

        this.selectable = this.Parent.GetComponent<Selectable>(Selectable);

        if (this.selectable != null)
        {
            this.sprite.inputEnabled = true;
        }
    }

    // Perform a simulation update
    protected Step(state: AbstractGameState, elapsedSeconds: number): void
    {
        // have the sprite follow the game object
        this.sprite.position.set(this.Parent.Position.X, this.Parent.Position.Y);
        this.sprite.rotation = this.Parent.Facing;

        if (this.selectable != null)
        {
            if (this.sprite.input.checkPointerDown(state.input.activePointer)
                && state.input.activePointer.leftButton.isDown)
            {

                this.selectable.SelectedByPointer = true;
            }
        }  
    }

    public LateStep(state: AbstractGameState, elapsedSeconds: number): void
    {
    }

    public Destroy(): void
    {
        if (this.sprite != null)
        {
            this.sprite.destroy();
            this.sprite = null;
        }
    }

    public ApplyUpdate(update: IComponentUpdate): void
    {
    }
}