﻿/// <reference path="AbstractComponent.ts" />

class Ownership extends AbstractComponent
{
    public ClientId: string;
    
    constructor(update: IComponentUpdate, parent: GameObject)
    {
        super(parent);

        this.ClientId = <string>update.Data[0];
    }
    
    protected Initialize(state: AbstractGameState): void
    {
    }

    protected Step(state: AbstractGameState, elapsedSeconds: number): void
    {
    }

    public LateStep(state: AbstractGameState, elapsedSeconds: number): void
    {
    }
    
    public Destroy(): void
    {
    }

    public ApplyUpdate(update: IComponentUpdate): void
    {
    }
}