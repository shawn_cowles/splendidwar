﻿class GameObject
{
    public Id: number;
    public Position: Vector2;
    public Facing: number;
    public IsRemoved: boolean;

    private components: AbstractComponent[];

    private lerpTimer: number;
    private currentUpdate: IGameObjectUpdate;
    private nextUpdate: IGameObjectUpdate;

    constructor(update: IGameObjectUpdate)
    {
        this.Id = update.Id;
        this.Position = Vector2.FromPosition(update.Position);
        this.Facing = update.Facing;
        this.currentUpdate = update;
        this.nextUpdate = update;
        this.lerpTimer = 0;

        this.IsRemoved = false;
        this.components = [];

        this.ApplyUpdate(update);
    }

    public Step(state: AbstractGameState, elapsedSeconds: number): void
    {
        this.lerpTimer += elapsedSeconds;

        if (this.nextUpdate.Timestamp <= this.currentUpdate.Timestamp)
        {
            this.Position = Vector2.FromPosition(this.currentUpdate.Position);
            this.Facing = this.currentUpdate.Facing;
        }
        else
        {
            this.Position = Vector2.FromLerp(
                this.currentUpdate.Position,
                this.nextUpdate.Position,
                this.lerpTimer / (this.nextUpdate.Timestamp - this.currentUpdate.Timestamp));

            this.Facing = Util.Lerp(
                this.currentUpdate.Facing,
                this.nextUpdate.Facing,
                this.lerpTimer / (this.nextUpdate.Timestamp - this.currentUpdate.Timestamp));
        }

        for (var i = 0; i < this.components.length; i++)
        {
            // may be null, due to the array being sparse
            if (this.components[i] != null)
            {
                this.components[i].DoStep(state, elapsedSeconds);
            }
        }
    }

    public LateStep(state: AbstractGameState, elapsedSeconds: number): void
    {
        for (var i = 0; i < this.components.length; i++)
        {
            // may be null, due to the array being sparse
            if (this.components[i] != null)
            {
                this.components[i].LateStep(state, elapsedSeconds);
            }
        }
    }

    public ApplyUpdate(update: IGameObjectUpdate): void
    {
        this.currentUpdate = this.nextUpdate;
        this.nextUpdate = update;
        this.lerpTimer = 0;

        for (var i = 0; i < update.ComponentUpdates.length; i++)
        {
            var componentUpdate = update.ComponentUpdates[i];

            if (this.components[componentUpdate.Id] == null)
            {
                if (!componentUpdate.RemoveComponent)
                {
                    this.CreateComponent(componentUpdate);
                }
            }
            else
            {
                if (componentUpdate.RemoveComponent)
                {
                    this.RemoveComponent(this.components[componentUpdate.Id]);
                }
                else
                {
                    this.components[componentUpdate.Id].ApplyUpdate(componentUpdate);
                }
            }
        }
    }

    public Destroy(): void
    {
        for (var i = 0; i < this.components.length; i++)
        {
            // may be null, due to the array being sparse
            if (this.components[i] != null)
            {
                this.components[i].Destroy();
            }
        }
    }

    private CreateComponent(update: IComponentUpdate): void
    {
        this.components[update.Id] = AbstractComponent.Create(update, this);
    }

    private RemoveComponent(component: AbstractComponent): void
    {
        this.components[component.Id] = null;
        component.IsRemoved = true;
        component.Destroy();
    }

    public HasComponent(type: Function): boolean
    {
        return this.GetComponent<any>(type) != null;
    }

    public GetComponent<T extends AbstractComponent>(type: Function): T
    {
        for (var i = 0; i < this.components.length; i++)
        {
            if (this.components[i] instanceof type)
            {
                return <T>this.components[i];
            }
        }

        return null;
    }

    public GetComponents<T extends AbstractComponent>(type: Function): T[]
    {
        var found = new Array<T>();

        for (var i = 0; i < this.components.length; i++)
        {
            if (this.components[i] instanceof type)
            {
                found.push(<T>this.components[i]);
            }
        }

        return found;
    }
}