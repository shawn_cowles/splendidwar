﻿window.onload = () =>
{
    var game = new Phaser.Game("100%", "100%", Phaser.AUTO, document.getElementById('game_div'));
    game.state.add(BootState.NAME, BootState, false);
    game.state.add(LoadState.NAME, LoadState, false);
    game.state.add(MenuState.NAME, MenuState, false);
    game.state.add(GameplayState.NAME, GameplayState, false);

    game.state.start(BootState.NAME);
};