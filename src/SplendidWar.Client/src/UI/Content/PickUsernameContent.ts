﻿/// <reference path="AbstractMenuContent.ts" />

class PickUsernameContent extends AbstractMenuContent
{
    public BuildContent(state: AbstractGameState, panel: MenuPanel): void
    {
        panel.SetTitle("Choose a Name");
        
        var userNameInput = new TextInput(275, 200, 200, ServiceHolder.PlayerDataService.GetPlayerName());

        panel.LayoutObjects.push(userNameInput);
        
        panel.LayoutObjects.push(new TextButton(287, 240, "Ok", panel.BodyTextStyle,
            () =>
            {
                ServiceHolder.PlayerDataService.SetPlayerName(userNameInput.GetValue());
                ServiceHolder.CommunicationService.SendMessage(new UserConnectedMessage(
                    ServiceHolder.PlayerDataService.PlayerId(),
                    ServiceHolder.PlayerDataService.GetPlayerName()
                ));
                panel.SetContent(new PlayerLobbyContent());
            },
            () =>
            {
                return userNameInput.GetValue().length < 1
            }));
    }

    public Update(state: AbstractGameState, panel: MenuPanel, elapsedSeconds: number): void
    {
    }

    public Destroy(): void
    {
    }
}