﻿/// <reference path="AbstractMenuContent.ts" />

class PlayerLobbyContent extends AbstractMenuContent
{
    private playerListObject: LayoutText;
    private refreshTimer: number;

    private matchButtons: TextButton[];

    public BuildContent(state: AbstractGameState, panel: MenuPanel): void
    {
        panel.SetTitle("Lobby");

        this.matchButtons = [];
        
        panel.LayoutObjects.push(new TextButton(0, 0, "Start Game", panel.BodyTextStyle,
            () =>
            {
                this.StartMatch(state, panel);
            }));

        panel.LayoutObjects.push(new TextButton(0, 40, "Test Game", panel.BodyTextStyle,
            () =>
            {
                this.StartTestGame(state, panel);
            }));

        this.playerListObject = new LayoutText(500, 0, "", panel.BodyTextStyle);
        panel.LayoutObjects.push(this.playerListObject);

        ServiceHolder.CommunicationService.RegisterMessageHandler(
            "UserListMessage",
            (msg: IUserListMessage) =>
            {
                this.RefreshUserList(msg);
            });

        ServiceHolder.CommunicationService.RegisterMessageHandler(
            "MatchListReply",
            (msg: IMatchListReply) =>
            {
                this.RefreshMatchList(msg, panel);
            });
        
        this.refreshTimer = 0;
    }

    public Update(state: AbstractGameState, panel: MenuPanel, elapsedSeconds: number): void
    {
        this.refreshTimer -= elapsedSeconds;

        if (this.refreshTimer <= 0)
        {
            ServiceHolder.CommunicationService.SendMessage(new UserListRequest());
            ServiceHolder.CommunicationService.SendMessage(new MatchListRequest());
            this.refreshTimer = 1;
        }
    }

    public Destroy(): void
    {
        ServiceHolder.CommunicationService.UnregisterMessageHandler("UserListMessage");
        ServiceHolder.CommunicationService.UnregisterMessageHandler("MatchListReply");
        ServiceHolder.CommunicationService.UnregisterMessageHandler("MatchSetupUpdate");
        ServiceHolder.CommunicationService.UnregisterMessageHandler("MatchStartingMessage");
    }

    private StartMatch(state: AbstractGameState, panel: MenuPanel): void
    {
        ServiceHolder.CommunicationService.RegisterMessageHandler(
            "MatchSetupUpdate",
            (msg: IMatchSetupUpdate) =>
            {
                panel.SetContent(new MatchLobbyContent(msg));
            });

        ServiceHolder.CommunicationService.SendMessage(new StartMatchSetupRequest());
    }

    private StartTestGame(state: AbstractGameState, panel: MenuPanel): void
    {
        panel.SetContent(new WaitingForServerContent("Starting game..."));

        ServiceHolder.CommunicationService.RegisterMessageHandler("MatchStartingMessage",
            (msg: IMatchStartingMessage) =>
            {
                state.game.state.start(GameplayState.NAME, true, false, msg);
            });

        ServiceHolder.CommunicationService.SendMessage(new StartQuickMatchRequest());
    }

    private RefreshUserList(msg: IUserListMessage): void
    {
        var text = msg.Count + " Players online:\n";

        if (msg.Count == 1)
        {
            text = "1 Player online:\n";
        }

        for (var i = 0; i < msg.Usernames.length; i++)
        {
            text += Util.ClampText(msg.Usernames[i], 25) + "\n";
        }

        this.playerListObject.SetText(text);
    }

    private RefreshMatchList(msg: IMatchListReply, panel: MenuPanel): void
    {
        for (var i = 0; i < this.matchButtons.length; i++)
        {
            this.matchButtons[i].Destroy();
        }
        this.matchButtons = [];

        for (var i = 0; i < msg.Matches.length; i++)
        {
            // Becuase javascript scoping
            var button = this.CreateJoinMatchButton(msg.Matches[i], i, panel);

            this.matchButtons.push(button);
            panel.LayoutObjects.push(button);
        }
    }

    private CreateJoinMatchButton(match: IMatchUnderSetupVm, i: number, panel: MenuPanel): TextButton
    {
        return new TextButton(
            250,
            i * (TextButton.HEIGHT + 5),
            Util.ClampText(match.Name, 17),
            panel.BodyTextStyle,
            () =>
            {
                ServiceHolder.CommunicationService.RegisterMessageHandler(
                    "MatchSetupUpdate",
                    (msg: IMatchSetupUpdate) =>
                    {
                        panel.SetContent(new MatchLobbyContent(msg));
                    });

                ServiceHolder.CommunicationService
                    .SendMessage(new JoinMatchRequest(match.Id));
            });
    }
}