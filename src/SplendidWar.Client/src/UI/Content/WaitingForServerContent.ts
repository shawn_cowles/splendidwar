﻿/// <reference path="AbstractMenuContent.ts" />

class WaitingForServerContent extends AbstractMenuContent
{
    private message: string;

    constructor(message: string)
    {
        super();

        this.message = message;
    }

    public BuildContent(state: AbstractGameState, panel: MenuPanel): void
    {
        panel.SetTitle("Waiting for Server");

        var text = new LayoutText(385, 200, this.message, panel.BodyTextStyle);
        text.Centered = true;
        panel.LayoutObjects.push(text);
    }

    public Update(state: AbstractGameState, panel: MenuPanel, elapsedSeconds: number): void
    {
    }

    public Destroy(): void
    {
    }
}