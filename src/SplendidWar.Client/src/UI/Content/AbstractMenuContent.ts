﻿abstract class AbstractMenuContent
{
    public abstract BuildContent(state: AbstractGameState, panel: MenuPanel): void;

    public abstract Update(state: AbstractGameState, panel: MenuPanel, elapsedSeconds: number): void;

    public abstract Destroy(): void;
}