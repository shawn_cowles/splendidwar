﻿/// <reference path="AbstractMenuContent.ts" />

class ServerCheckContent extends AbstractMenuContent
{
    private recheckTimer: number;

    public BuildContent(state: AbstractGameState, panel: MenuPanel): void
    {
        panel.SetTitle("Checking Server");
        
        panel.LayoutObjects.push(new LayoutText(275, 200, "Cannot Reach Server", panel.BodyTextStyle));

        this.recheckTimer = 5;
    }

    public Update(state: AbstractGameState, panel: MenuPanel, elapsedSeconds: number): void
    {
        if (ServiceHolder.CommunicationService.IsConnected())
        {
            panel.SetContent(new PickUsernameContent());
        }

        this.recheckTimer -= elapsedSeconds;
        if (this.recheckTimer <= 0)
        {
            this.recheckTimer = 5;
            ServiceHolder.CommunicationService.TryConnect();
        }

    }

    public Destroy(): void
    {
    }
}