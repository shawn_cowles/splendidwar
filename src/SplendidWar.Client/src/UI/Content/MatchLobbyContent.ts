﻿/// <reference path="AbstractMenuContent.ts" />

class MatchLobbyContent extends AbstractMenuContent
{
    private match: IMatchUnderSetupVm;

    private slotTexts: BorderedText[];
    private slotKickButtons: TextButton[];
    private slotBotButtons: TextButton[];
    private startButton: TextButton;
    private validationText: LayoutText;

    private isHost: boolean;
    constructor(msg: IMatchSetupUpdate)
    {
        super();

        this.match = msg.Match;
        this.isHost = msg.Match.HostId == ServiceHolder.CommunicationService.OwnClientId;
    }

    public BuildContent(state: AbstractGameState, panel: MenuPanel): void
    {
        panel.SetTitle(this.match.Name);

        this.slotTexts = [];
        this.slotKickButtons = [];
        this.slotBotButtons = [];

        for (var i = 0; i < this.match.Slots.length; i++)
        {
            this.BuildControlsForSlot(i, panel);
        }

        panel.LayoutObjects.push(new TextButton(0, 520, "Leave", panel.BodyTextStyle,
            () =>
            {
                ServiceHolder.CommunicationService.SendMessage(new LeaveMatchSetupMessage());
                panel.SetContent(new PlayerLobbyContent());
            }));

        if (this.isHost)
        {
            this.startButton = new TextButton(565, 520, "Start", panel.BodyTextStyle,
                () =>
                {
                    ServiceHolder.CommunicationService
                        .SendMessage(new MatchSetupFinishedMessage(this.match.Id));
                });
            panel.LayoutObjects.push(this.startButton);

            this.validationText = new LayoutText(380, 490, "", panel.BodyTextStyle);
            panel.LayoutObjects.push(this.validationText);
        }

        ServiceHolder.CommunicationService.RegisterMessageHandler(
            "MatchSetupUpdate",
            (msg: IMatchSetupUpdate) =>
            {
                this.match = msg.Match;
                this.RefreshSlots();
            });

        ServiceHolder.CommunicationService.RegisterMessageHandler(
            "KickedFromMatchMessage",
            (msg: IKickedFromMatchMessage) =>
            {
                panel.SetContent(new PlayerLobbyContent());
            });

        ServiceHolder.CommunicationService.RegisterMessageHandler(
            "MatchStartingMessage",
            (msg: IMatchStartingMessage) =>
            {
                state.game.state.start(GameplayState.NAME, true, false, msg);
            });

        this.RefreshSlots();
    }

    public Update(state: AbstractGameState, panel: MenuPanel, elapsedSeconds: number): void
    {
        if (this.isHost)
        {
            var slotsFilled = 0;

            for (var i = 0; i < this.match.Slots.length; i++)
            {
                if (this.match.Slots[i].HasBot || this.match.Slots[i].HasPlayer)
                {
                    slotsFilled += 1;
                }
            }

            this.startButton.IsEnabled = slotsFilled > 1;

            if (slotsFilled < 2)
            {
                this.validationText.SetText("Must have at least 2 players or bots.");
            }
            else
            {
                this.validationText.SetText("");
            }
        }
    }

    public Destroy(): void
    {
        ServiceHolder.CommunicationService.UnregisterMessageHandler("MatchSetupUpdate");
    }

    private BuildControlsForSlot(slotNumber: number, panel: MenuPanel): void
    {
        var baseY = 70 * slotNumber;

        var text = new BorderedText(0, baseY, "", panel.BodyTextStyle, 200, 20);
        this.slotTexts.push(text);
        panel.LayoutObjects.push(text);

        var kickButton = new TextButton(210, baseY - 7, "Kick", panel.BodyTextStyle,
            () =>
            {
                this.ClearSlot(slotNumber);
            });
        this.slotKickButtons.push(kickButton);
        panel.LayoutObjects.push(kickButton);

        var botButton = new TextButton(210, baseY - 7, "Add Bot", panel.BodyTextStyle,
            () =>
            {
                this.AddBot(slotNumber);
            });
        kickButton.IsVisible = false;
        this.slotBotButtons.push(botButton);
        panel.LayoutObjects.push(botButton);
    }

    private ClearSlot(slotNumber: number): void
    {
        var msg = new SlotChangeMessage();
        msg.ClearSlot = true;
        msg.MatchId = this.match.Id;
        msg.SlotIndex = slotNumber;

        ServiceHolder.CommunicationService.SendMessage(msg);
    }

    private AddBot(slotNumber: number): void
    {
        var msg = new SlotChangeMessage();
        msg.SetBot = true;
        msg.MatchId = this.match.Id;
        msg.SlotIndex = slotNumber;

        ServiceHolder.CommunicationService.SendMessage(msg);
    }

    private RefreshSlots(): void
    {
        for (var i = 0; i < this.match.Slots.length; i++)
        {
            var slot = this.match.Slots[i];

            if (slot.HasBot)
            {
                this.slotTexts[i].SetText("Bot");
                this.slotKickButtons[i].IsVisible = this.isHost;
                this.slotBotButtons[i].IsVisible = false;
            }
            else if (slot.HasPlayer)
            {
                this.slotTexts[i].SetText(slot.PlayerName);

                this.slotKickButtons[i].IsVisible = this.isHost && slot.PlayerId != ServiceHolder.CommunicationService.OwnClientId;
                this.slotBotButtons[i].IsVisible = false;
            }
            else
            {
                this.slotTexts[i].SetText("Empty");

                this.slotKickButtons[i].IsVisible = false;
                this.slotBotButtons[i].IsVisible = this.isHost;
            }
        }
    }
}