﻿/// <reference path="UiPanel.ts" />

// A widget that displays a large menu dialog in the center of the screen.
class MenuPanel extends UiPanel
{
    public static MARGIN_LEFT = 15;
    public static MARGIN_TOP = 45;

    public TitleTextStyle: any;
    public BodyTextStyle: any;
    
    private content: AbstractMenuContent;
    private contentBuilt: boolean;

    private title: Phaser.Text;
    public LayoutObjects: LayoutObject[];

    constructor(width: number, height: number, content: AbstractMenuContent)
    {
        super(new Phaser.Point(0, 0), width, height);

        this.content = content;
    }

    // Initialize the widget before its first update
    protected Initialize(state: AbstractGameState): void
    {
        super.Initialize(state);
        
        this.BodyTextStyle = {
            font: "18px courier",
            fill: "#00FFFF",
            align: "left",
        };
        
        this.TitleTextStyle = {
            font: "24px courier",
            fill: "#00FFFF",
            align: "center",
        };

        this.LayoutObjects = [];
        this.title = state.add.text(0, 0, "", this.TitleTextStyle, state.OverlaySprites);
        this.title.anchor.set(0.5, 0.5);
    }

    // Update position and show/hide
    protected Update(state: AbstractGameState, elapsedSeconds: number): void
    {
        this.CenterPosition = new Phaser.Point(
            state.game.camera.width / 2 + state.game.camera.view.x,
            state.game.camera.height / 2 + state.game.camera.view.y);
        
        super.Update(state, elapsedSeconds);


        if (!this.contentBuilt)
        {
            this.content.BuildContent(state, this);
            
            this.contentBuilt = true;
        }

        this.title.position.set(
            this.CenterPosition.x,
            this.CenterPosition.y - this.Height / 2 + 15);
        
        var upperLeft = new Phaser.Point(
            this.CenterPosition.x - this.Width / 2 + MenuPanel.MARGIN_LEFT,
            this.CenterPosition.y - this.Height / 2 + MenuPanel.MARGIN_TOP);

        for (var i = 0; i < this.LayoutObjects.length; i++)
        {
            if (!this.LayoutObjects[i].Initialized)
            {
                this.LayoutObjects[i].Initialized = true;
                this.LayoutObjects[i].Initialize(state);
            }

            this.LayoutObjects[i].Update(upperLeft, this.Width, this.Height);
        }

        this.content.Update(state, this, elapsedSeconds);
    }

    public Destroy()
    {
        super.Destroy();

        this.title.destroy();
        this.DestroyContent();
    }

    public DestroyContent(): void
    {
        if (this.content != null)
        {
            this.content.Destroy();
        }

        for (var i = 0; i < this.LayoutObjects.length; i++)
        {
            this.LayoutObjects[i].Destroy();
        }

        this.LayoutObjects = [];
    }

    public SetContent(content: AbstractMenuContent): void
    {
        this.DestroyContent();
        this.contentBuilt = false;
        this.content = content;
    }

    public SetTitle(text: string): void
    {
        this.title.text = text;
    }
}