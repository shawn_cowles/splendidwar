﻿/// <reference path="AbstractUiWidget.ts" />

// Base class for UiWidgets that use the sliced window.
abstract class UiPanel extends AbstractUiWidget
{
    public static SLICE_SIZE = 15;

    public CenterPosition: Phaser.Point;

    protected Width: number;
    protected Height: number;

    private ulSprite: Phaser.Sprite;
    private topSprite: Phaser.Sprite;
    private urSprite: Phaser.Sprite;
    private leftSprite: Phaser.Sprite;
    private centerSprite: Phaser.Sprite;
    private rightSprite: Phaser.Sprite;
    private llSprite: Phaser.Sprite;
    private bottomSprite: Phaser.Sprite;
    private lrSprite: Phaser.Sprite;

    constructor(center: Phaser.Point, width: number, height: number)
    {
        super();

        this.CenterPosition = center;
        this.Width = width;
        this.Height = height;
    }

    // Create the sprites for the panel
    protected Initialize(state: AbstractGameState): void
    {
        this.ulSprite = state.game.add.sprite(0, 0, "sliced_window", 0,
            state.OverlaySprites);
        this.ulSprite.anchor.set(0.5, 0.5);
        this.topSprite = state.game.add.sprite(0, 0, "sliced_window", 1,
            state.OverlaySprites);
        this.topSprite.anchor.set(0.5, 0.5);
        this.urSprite = state.game.add.sprite(0, 0, "sliced_window", 2,
            state.OverlaySprites);
        this.urSprite.anchor.set(0.5, 0.5);
        this.leftSprite = state.game.add.sprite(0, 0, "sliced_window", 3,
            state.OverlaySprites);
        this.leftSprite.anchor.set(0.5, 0.5);
        this.centerSprite = state.game.add.sprite(0, 0, "sliced_window", 4,
            state.OverlaySprites);
        this.centerSprite.anchor.set(0.5, 0.5);
        this.rightSprite = state.game.add.sprite(0, 0, "sliced_window", 5,
            state.OverlaySprites);
        this.rightSprite.anchor.set(0.5, 0.5);
        this.llSprite = state.game.add.sprite(0, 0, "sliced_window", 6,
            state.OverlaySprites);
        this.llSprite.anchor.set(0.5, 0.5);
        this.bottomSprite = state.game.add.sprite(0, 0, "sliced_window", 7,
            state.OverlaySprites);
        this.bottomSprite.anchor.set(0.5, 0.5);
        this.lrSprite = state.game.add.sprite(0, 0, "sliced_window", 8,
            state.OverlaySprites);
        this.lrSprite.anchor.set(0.5, 0.5);
    }

    // Update sprite positions around the center
    protected Update(state: AbstractGameState, elapsedSeconds: number): void
    {
        this.ForceUpdatePosition();
    }

    public ForceUpdatePosition(): void
    {
        var hScale = (this.Width - UiPanel.SLICE_SIZE) / UiPanel.SLICE_SIZE;
        var vScale = (this.Height - UiPanel.SLICE_SIZE) / UiPanel.SLICE_SIZE;

        this.ulSprite.position.set(
            this.CenterPosition.x - this.Width / 2,
            this.CenterPosition.y - this.Height / 2);

        this.topSprite.position.set(
            this.CenterPosition.x,
            this.CenterPosition.y - this.Height / 2);
        this.topSprite.scale.set(hScale, 1);

        this.urSprite.position.set(
            this.CenterPosition.x + this.Width / 2,
            this.CenterPosition.y - this.Height / 2);


        this.leftSprite.position.set(
            this.CenterPosition.x - this.Width / 2,
            this.CenterPosition.y);
        this.leftSprite.scale.set(1, vScale);

        this.centerSprite.position.set(
            this.CenterPosition.x,
            this.CenterPosition.y);
        this.centerSprite.scale.set(hScale, vScale);

        this.rightSprite.position.set(
            this.CenterPosition.x + this.Width / 2,
            this.CenterPosition.y);
        this.rightSprite.scale.set(1, vScale);


        this.llSprite.position.set(
            this.CenterPosition.x - this.Width / 2,
            this.CenterPosition.y + this.Height / 2);

        this.bottomSprite.position.set(
            this.CenterPosition.x,
            this.CenterPosition.y + this.Height / 2);
        this.bottomSprite.scale.set(hScale, 1);

        this.lrSprite.position.set(
            this.CenterPosition.x + this.Width / 2,
            this.CenterPosition.y + this.Height / 2);
    }

    public Destroy()
    {
        this.ulSprite.destroy();
        this.topSprite.destroy();
        this.urSprite.destroy();
        this.leftSprite.destroy();
        this.centerSprite.destroy();
        this.rightSprite.destroy();
        this.llSprite.destroy();
        this.bottomSprite.destroy();
        this.lrSprite.destroy();
    }

    public SetVisible(visible: boolean): void
    {
        this.ulSprite.visible = visible;
        this.topSprite.visible = visible;
        this.urSprite.visible = visible;
        this.leftSprite.visible = visible;
        this.centerSprite.visible = visible;
        this.rightSprite.visible = visible;
        this.llSprite.visible = visible;
        this.bottomSprite.visible = visible;
        this.lrSprite.visible = visible;
    }

    // Is the pointer over the panel?
    public PointIsInWidget(point: Vector2): boolean
    {
        if (this.centerSprite == null || !this.centerSprite.visible)
        {
            return false;
        }

        return this.CenterPosition.x - this.Width / 2 <= point.X
            && point.X <= this.CenterPosition.x + this.Width / 2
            && this.CenterPosition.y - this.Height / 2 <= point.Y
            && point.Y <= this.CenterPosition.y + this.Height / 2;
    }
}