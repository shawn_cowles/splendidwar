﻿/// <reference path="AbstractUiWidget.ts" />

class MiniMap extends AbstractUiWidget
{
    static MINIMAP_SIZE = 200;

    private graphics: Phaser.Graphics;
    private backingSprite: Phaser.Sprite;

    private width: number;
    private height: number;
    private borderThickness: number;
    private ownClientId: string;

    constructor()
    {
        super();

        this.ownClientId = ServiceHolder.CommunicationService.OwnClientId;
        this.width = MiniMap.MINIMAP_SIZE;
        this.height = MiniMap.MINIMAP_SIZE;
        this.borderThickness = 2;
    }

    // Initialize the widget before its first update
    public Initialize(state: AbstractGameState): void
    {
        this.backingSprite = state.game.add.sprite(0, 0);
        this.backingSprite.width = MiniMap.MINIMAP_SIZE;
        this.backingSprite.height = MiniMap.MINIMAP_SIZE;
        this.backingSprite.inputEnabled = true;

        this.graphics = state.game.add.graphics(0, 0);
    }

    // Update the minimap
    public Update(state: AbstractGameState, elapsedSeconds: number): void
    {
        this.graphics.clear();

        this.DoMinimapMovement(state);

        var xPosition = state.game.camera.width - this.width + state.game.camera.view.x;
        var yPosition = state.game.camera.view.y;

        this.graphics.position.set(xPosition, yPosition);

        this.backingSprite.position.set(xPosition, yPosition);

        this.DrawBackground();

        var xScale = this.width / state.world.width;
        var yScale = this.height / state.world.height;

        this.DrawViewport(state, xScale, yScale);

        this.DrawObjects(state, xScale, yScale);

        this.DrawBoder();
    }

    // Is the pointer over the minimap?
    public PointIsInWidget(point: Vector2): boolean
    {
        if (this.backingSprite == null)
        {
            return false;
        }

        return this.backingSprite.position.x <= point.X
            && point.X <= this.backingSprite.position.x + this.width
            && this.backingSprite.position.y <= point.Y
            && point.Y <= this.backingSprite.position.y + this.height;
    }

    private DrawBackground(): void
    {
        this.graphics.moveTo(0, 0);
        this.graphics.beginFill(HexColor.WINDOW_BACKGROUND, 0.5);
        this.graphics.drawRect(0, 0, this.width, this.height);
        this.graphics.endFill();
    }

    private DrawBoder(): void
    {
        this.graphics.lineStyle(this.borderThickness, HexColor.WINDOW_BORDER, 1);

        this.graphics.drawRect(
            0,
            0,
            this.width,
            this.height);
    }

    private DrawViewport(state: AbstractGameState, xScale: number, yScale: number): void
    {
        this.graphics.lineStyle(1, HexColor.WHITE, 1);

        var x = state.game.camera.x * xScale;
        var y = state.game.camera.y * yScale;

        var xUnderage = Math.min(0, x) * -1;
        var yUnderage = Math.min(0, y) * -1;

        var width = state.game.camera.width * xScale;
        width = width - Math.max(0, x + width - this.width) - xUnderage;

        var height = state.game.camera.height * yScale;
        height = height - Math.max(0, y + height - this.height) - yUnderage;


        x = Math.max(0, Math.min(this.width, x));
        y = Math.max(0, Math.min(this.height, y));

        this.graphics.drawRect(x, y, width, height);
    }

    private DoMinimapMovement(state: AbstractGameState): void
    {
        if (state.input.activePointer.leftButton.isDown)
        {
            var point = state.input.activePointer.position;
            
            var localX = point.x - state.camera.width + this.width;
            var localY = point.y;

            if (0 <= localX && localX < this.width
                && 0 <= localY && localY < this.height)
            {
                var centerX = localX / this.width * state.world.width;
                var centerY = localY / this.height * state.world.height;

                state.camera.focusOnXY(centerX, centerY);
            }
        }
    }


    private DrawObjects(state: AbstractGameState, xScale: number, yScale: number): void
    {
        this.graphics.lineStyle(1, 0xFFFFFF, 1);

        for (var i = 0; i < state.GameObjects.length; i++)
        {
            var obj = state.GameObjects[i];

            var x = obj.Position.X * xScale;
            var y = obj.Position.Y * yScale;

            this.DrawShip(x, y, obj);

            //var minimapDisplay = obj.GetComponent<MinimapDisplay>(MinimapDisplay);

            //if (minimapDisplay != null)
            //{
            //    switch (minimapDisplay.Type)
            //    {
            //        case MinimapDisplay.TYPE_PROJECTILE:
            //            this.DrawProjectile(x, y);
            //            break;
            //        case MinimapDisplay.TYPE_SHIP:
            //            this.DrawShip(x, y, obj);
            //            break;
            //        case MinimapDisplay.TYPE_TERRAIN:
            //            this.DrawTerrain(x, y);
            //            break;
            //        case MinimapDisplay.TYPE_NAVIGATION:
            //            this.DrawNavigation(x, y);
            //            break;
            //        case MinimapDisplay.TYPE_STATION:
            //            this.DrawStation(x, y);
            //            break;
            //        default:
            //            throw "Unknown minimap type: " + minimapDisplay.Type;
            //    }
            //}
        }
    }
    
    private DrawShip(x: number, y: number, ship: GameObject): void
    {
        var ownership = ship.GetComponent<Ownership>(Ownership);

        var color = HexColor.WHITE;

        if (ownership != null)
        {
            if (ownership.ClientId == this.ownClientId)
            {
                color = HexColor.GREEN;
            }
            else
            {
                color = HexColor.RED;
            }
        }

        this.graphics.lineStyle(1, color, 1);
        this.graphics.beginFill(color, 1);
        this.graphics.drawCircle(x, y, 6);
        this.graphics.endFill();
    }

    private DrawTerrain(x: number, y: number): void
    {
        var color = HexColor.WHITE;

        this.graphics.lineStyle(1, color, 1);
        this.graphics.beginFill(color, 1);
        this.graphics.drawCircle(x, y, 6);
        this.graphics.endFill();
    }

    private DrawNavigation(x: number, y: number): void
    {
        var outerRadius = 6;
        var innerRadius = 2;

        this.graphics.lineStyle(2, HexColor.YELLOW, 1);
        this.graphics.moveTo(x, y - outerRadius);
        this.graphics.lineTo(x + innerRadius, y - innerRadius);
        this.graphics.lineTo(x + outerRadius, y);
        this.graphics.lineTo(x + innerRadius, y + innerRadius);
        this.graphics.lineTo(x, y + outerRadius);
        this.graphics.lineTo(x - innerRadius, y + innerRadius);
        this.graphics.lineTo(x - outerRadius, y);
        this.graphics.lineTo(x - innerRadius, y - innerRadius);
        this.graphics.lineTo(x, y - outerRadius);
    }

    private DrawStation(x: number, y: number): void
    {
        var radius = 4;

        this.graphics.lineStyle(2, HexColor.YELLOW, 1);
        this.graphics.moveTo(x - radius, y - radius);
        this.graphics.lineTo(x + radius, y - radius);
        this.graphics.lineTo(x + radius, y + radius);
        this.graphics.lineTo(x - radius, y + radius);
        this.graphics.lineTo(x - radius, y - radius);
    }

    public Destroy()
    {
        this.graphics.destroy();
        this.backingSprite.destroy();
    }
}