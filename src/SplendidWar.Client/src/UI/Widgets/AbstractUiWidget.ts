﻿// The base class for any UI elements.
abstract class AbstractUiWidget
{
    private isInitialized: boolean;

    constructor()
    {
        this.isInitialized = false;
    }
    
    public DoUpdate(state: AbstractGameState, elapsedSeconds: number): void
    {
        if (!this.isInitialized)
        {
            this.isInitialized = true;
            this.Initialize(state);
        }

        this.Update(state, elapsedSeconds);
    }
    
    protected abstract Initialize(state: AbstractGameState): void;

    protected abstract Update(state: AbstractGameState, elapsedSeconds: number): void;
    
    public abstract PointIsInWidget(point: Vector2): boolean;
    
    public abstract Destroy(): void;
}