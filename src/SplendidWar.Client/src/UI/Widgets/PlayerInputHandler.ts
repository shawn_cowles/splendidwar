﻿/// <reference path="AbstractUiWidget.ts" />

class PlayerInputHandler extends AbstractUiWidget
{
    private selection: GameObject[];
    private leftWasDown: boolean;
    private rightWasDown: boolean;
    private dragStart: Vector2;
    private isDragging: boolean;
    private dragCounter: number;

    protected Initialize(state: AbstractGameState): void
    {
        this.selection = [];
    }

    protected Update(state: AbstractGameState, elapsedSeconds: number): void
    {
        this.HandleSelecting(state);

        this.DrawSelections(state);

        this.HandleQuickOrders(state);

        if (!state.PointerInUi)
        {
            this.leftWasDown = state.input.activePointer.leftButton.isDown;
            this.rightWasDown = state.input.activePointer.rightButton.isDown;
        }
        else
        {
            this.leftWasDown = false;
            this.rightWasDown = false;
        }
    }
    
    public PointIsInWidget(point: Vector2): boolean
    {
        return false;
    }
    
    public Destroy(): void
    {
    }

    private HandleSelecting(state: AbstractGameState): void
    {
        if (state.PointerInUi)
        {
            return;
        }

        var pointerWorldPos = new Vector2(
            state.input.activePointer.worldX,
            state.input.activePointer.worldY);

        if (!this.leftWasDown)
        {
            if (state.input.activePointer.leftButton.isDown && !state.PointerInUi)
            {
                this.selection = [];
                this.dragStart = pointerWorldPos;
            }

            for (var i = 0; i < state.GameObjects.length; i++)
            {
                if (state.GameObjects[i] != null)
                {
                    var selector = state.GameObjects[i].GetComponent<Selectable>(Selectable);

                    if (selector != null && selector.SelectedByPointer)
                    {
                        this.selection = [state.GameObjects[i]];
                    }
                }
            }
        }

        if (this.leftWasDown && this.dragStart.DistanceTo(pointerWorldPos) > 5)
        {
            // Do box select
            if (!state.input.activePointer.leftButton.isDown)
            {
                var minX = Math.min(this.dragStart.X, pointerWorldPos.X);
                var maxX = Math.max(this.dragStart.X, pointerWorldPos.X);
                var minY = Math.min(this.dragStart.Y, pointerWorldPos.Y);
                var maxY = Math.max(this.dragStart.Y, pointerWorldPos.Y);

                for (var i = 0; i < state.GameObjects.length; i++)
                {
                    var obj = state.GameObjects[i];
                    if (obj != null)
                    {
                        if (minX < obj.Position.X && obj.Position.X < maxX
                            && minY < obj.Position.Y && obj.Position.Y < maxY)
                        {
                            var ownership = obj.GetComponent<Ownership>(Ownership);

                            if (ownership != null && ownership.ClientId == ServiceHolder.CommunicationService.OwnClientId)
                            {
                                this.selection.push(obj);
                            }
                        }
                    }
                }
            }

            // Draw selection box
            if (state.input.activePointer.leftButton.isDown)
            {
                state.OverlayGraphics.lineStyle(1, HexColor.WHITE, 1);
                state.OverlayGraphics.drawRect(
                    Math.min(this.dragStart.X, pointerWorldPos.X),
                    Math.min(this.dragStart.Y, pointerWorldPos.Y),
                    Math.max(this.dragStart.X, pointerWorldPos.X) - Math.min(this.dragStart.X, pointerWorldPos.X),
                    Math.max(this.dragStart.Y, pointerWorldPos.Y) - Math.min(this.dragStart.Y, pointerWorldPos.Y));
            }
        }
    }

    private DrawSelections(state: AbstractGameState): void
    {
        if (this.selection != null)
        {
            for (var i = 0; i < this.selection.length; i++)
            {
                var obj = this.selection[i];

                var lineColor = HexColor.GREEN;

                if (this.selection[i].GetComponent<Ownership>(Ownership).ClientId != ServiceHolder.CommunicationService.OwnClientId)
                {
                    lineColor = HexColor.RED;
                }

                state.OverlayGraphics.lineStyle(3, lineColor, 1);
                state.OverlayGraphics.drawCircle(
                    obj.Position.X,
                    obj.Position.Y,
                    50);
            }
        }
    }

    private HandleQuickOrders(state: AbstractGameState): void
    {
        if (this.selection.length > 0
            && state.input.activePointer.rightButton.isDown
            && !this.rightWasDown
            && !state.PointerInUi)
        {
            var orderedObjectIds = new Array<number>();

            for (var i = 0; i < this.selection.length; i++)
            {
                var owner = this.selection[i].GetComponent<Ownership>(Ownership);

                if (owner != null && owner.ClientId == ServiceHolder.CommunicationService.OwnClientId)
                {
                    orderedObjectIds.push(this.selection[i].Id);
                }
            }

            var targetPosition = new Vector2(
                state.input.activePointer.worldX,
                state.input.activePointer.worldY);

            if (orderedObjectIds.length > 0)
            {
                ServiceHolder.CommunicationService.SendMessage(new IssueOrderMessage(
                    orderedObjectIds,
                    SplendidWar.Contracts.Data.OrderType.Move,
                    [],
                    { X: targetPosition.X, Y: targetPosition.Y }));
            }
        }
    }
}