﻿/// <reference path="LayoutText.ts" />

class BorderedText extends LayoutText
{
    private width: number;
    private height: number;

    private state: AbstractGameState;

    constructor(left: number, top: number, content: string, style: any, width: number, height: number)
    {
        super(left,top,content,style);
        
        this.width = width;
        this.height = height;
    }

    public Initialize(state: AbstractGameState): void
    {
        super.Initialize(state);
        this.state = state;
    }

    public Update(upperLeft: Phaser.Point, width: number, height: number): void
    {
        super.Update(upperLeft, width, height);

        var startX = upperLeft.x + this.left - 2;
        var startY = upperLeft.y + this.top - 2;
        
        this.state.OverlayGraphics.lineStyle(2, 0x3E81FF, 1);
        this.state.OverlayGraphics.moveTo(startX, startY);
        this.state.OverlayGraphics.lineTo(startX + this.width, startY);
        this.state.OverlayGraphics.lineTo(startX + this.width, startY + this.height);
        this.state.OverlayGraphics.lineTo(startX, startY + this.height);
        this.state.OverlayGraphics.lineTo(startX, startY);
        
    }

    public Destroy(): void
    {
        super.Destroy()
        this.state.OverlayGraphics.destroy();
    }
}