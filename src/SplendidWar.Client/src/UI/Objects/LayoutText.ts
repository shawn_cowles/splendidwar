﻿/// <reference path="LayoutObject.ts" />

class LayoutText extends LayoutObject
{
    protected left: number;
    protected top: number;
    private content: string;
    private style: any;

    public Centered: boolean;

    private textObject: Phaser.Text;

    constructor(left: number, top: number, content: string, style: any)
    {
        super();
        
        this.left = left;
        this.top = top;
        this.content = content;
        this.style = style;
        this.Centered = false;
    }

    public Initialize(state: AbstractGameState): void
    {
        this.textObject = state.add.text(0, 0, this.content, this.style, state.OverlaySprites);
    }

    public Update(upperLeft: Phaser.Point, width: number, height: number): void
    {
        if (this.Centered)
        {
            this.textObject.anchor.set(0.5, 0);
        }

        this.textObject.position.set(
            upperLeft.x + this.left,
            upperLeft.y + this.top);
    }

    public Destroy(): void
    {
        this.textObject.destroy();
    }

    public SetText(newText: string): void
    {
        if (this.textObject != null)
        {
            this.textObject.text = newText;
        }
        else
        {
            this.content = newText;
        }
    }
}