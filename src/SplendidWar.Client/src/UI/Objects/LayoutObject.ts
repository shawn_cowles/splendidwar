﻿abstract class LayoutObject
{
    public Initialized: boolean;

    public abstract Initialize(state: AbstractGameState): void;

    public abstract Update(upperLeft: Phaser.Point, width: number, height: number): void;

    public abstract Destroy(): void;
}