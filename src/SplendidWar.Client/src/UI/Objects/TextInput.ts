﻿/// <reference path="LayoutObject.ts" />

class TextInput extends LayoutObject
{
    private left: number;
    private top: number;
    private width: number;
    private value: string;

    private input: HTMLInputElement;

    constructor(left: number, top: number, width: number, value: string)
    {
        super();

        this.left = left;
        this.top = top;
        this.width = width;
        this.value = value;
    }

    public Initialize(state: AbstractGameState): void
    {
        this.input = document.createElement("input");
        this.input.value = this.value;

        document
            .getElementById("game_div")
            .appendChild(this.input);

        this.input.focus();
    }

    public Update(upperLeft: Phaser.Point, width: number, height: number): void
    {
        this.input.style.left = (upperLeft.x + this.left) + "px";
        this.input.style.top = (upperLeft.y + this.top) + "px";
    }

    public Destroy(): void
    {
        document.getElementById("game_div").removeChild(this.input);
        this.input = null;
    }

    public GetValue(): string
    {
        if (this.input == null)
        {
            return "";
        }

        return this.input.value;
    }
}