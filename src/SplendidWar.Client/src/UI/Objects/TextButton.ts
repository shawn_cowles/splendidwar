﻿/// <reference path="LayoutObject.ts" />

class TextButton extends LayoutObject
{
    public static WIDTH = 200;
    public static HEIGHT = 30;

    private left: number;
    private top: number;
    private content: string;
    private style: any;
    private callback: () => void;
    private disableCheck: () => boolean;
    public IsVisible: boolean;
    public IsEnabled: boolean;

    private buttonObject: Phaser.Button;
    private textObject: Phaser.Text;

    constructor(left: number, top: number, content: string, style: any, callback: () => void,
        disableCheck?: () => boolean)
    {
        super();

        this.left = left;
        this.top = top;
        this.content = content;
        this.style = style;
        this.callback = callback;
        this.disableCheck = disableCheck;
        this.IsVisible = true;
        this.IsEnabled = true;
    }

    public Initialize(state: AbstractGameState): void
    {
        this.buttonObject = state.add.button(0, 0,
            "text_button",
            this.callback,
            null,
            1, 0, 1, 0,
            state.OverlaySprites);
        
        this.textObject = state.add.text(0, 0, this.content, this.style, state.OverlaySprites);
        //this.textObject.anchor.set(0.5, 0.5);
    }

    public Update(upperLeft: Phaser.Point, width: number, height: number): void
    {
        this.buttonObject.position.set(
            upperLeft.x + this.left,
            upperLeft.y + this.top);

        this.textObject.position.set(
            Math.round(upperLeft.x + this.left + TextButton.WIDTH / 2 - this.textObject.width / 2),
            Math.round(upperLeft.y + this.top + TextButton.HEIGHT / 2 - this.textObject.height / 2) + 3);

        if (this.disableCheck != null)
        {
            var disableInput = this.disableCheck();

            if (this.buttonObject.input.enabled && disableInput)
            {
                this.DisableButton();
            }

            if (!this.buttonObject.input.enabled && !disableInput)
            {
                this.EnableButton();
            }
        }

        if (this.IsEnabled != this.buttonObject.input.enabled)
        {
            if (this.IsEnabled)
            {
                this.EnableButton();
            }
            else
            {
                this.DisableButton();
            }
        }

        this.textObject.visible = this.IsVisible;
        this.buttonObject.visible = this.IsVisible;
    }

    public Destroy(): void
    {
        this.buttonObject.destroy();
        this.textObject.destroy();
    }

    private EnableButton(): void
    {
        this.buttonObject.input.enabled = true;
        this.buttonObject.frame = 0;
    }

    private DisableButton(): void
    {
        this.buttonObject.input.enabled = false;
        this.buttonObject.frame = 2;
    }
}