﻿
 
 

 

/// <reference path="Enums.ts" />

	interface IAbstractClientMessage extends IAbstractMessage {
		MessageName: string;
	}
	interface IAbstractIncomingClientMessage extends IAbstractClientMessage {
	}
	interface IAbstractMessage {
	}
	interface IAbstractOutgoingClientMessage extends IAbstractClientMessage {
	}
	interface IClientManifest {
		ShipImages: string[];
	}
	interface IComponentUpdate {
		ComponentName: string;
		Data: any[];
		Id: number;
		RemoveComponent: boolean;
	}
	interface IGameObjectUpdate {
		ComponentUpdates: IComponentUpdate[];
		Facing: number;
		Id: number;
		Position: IPosition;
		RemoveObject: boolean;
		Timestamp: number;
	}
	interface IIssueOrderMessage extends SplendidWar.Contracts.Messages.PlayerInputMessage {
		Order: SplendidWar.Contracts.Data.OrderType;
		OrderedObjectIds: number[];
		TargetIds: number[];
		TargetPositionX: number;
		TargetPositionY: number;
	}
	interface IJoinMatchRequest extends IAbstractIncomingClientMessage {
		MatchId: string;
	}
	interface IKickedFromMatchMessage extends IAbstractOutgoingClientMessage {
	}
	interface ILeaveMatchSetupMessage extends IAbstractIncomingClientMessage {
	}
	interface IMatchListReply extends IAbstractOutgoingClientMessage {
		Matches: IMatchUnderSetupVm[];
	}
	interface IMatchListRequest extends IAbstractIncomingClientMessage {
	}
	interface IMatchSetupFinishedMessage extends IAbstractIncomingClientMessage {
		MatchId: string;
	}
	interface IMatchSetupUpdate extends IAbstractOutgoingClientMessage {
		Match: IMatchUnderSetupVm;
	}
	interface IMatchStartingMessage extends IAbstractOutgoingClientMessage {
		MapHeight: number;
		MapWidth: number;
		MatchId: string;
	}
	interface IMatchStepUpdateMessage extends IAbstractOutgoingClientMessage {
		Updates: IGameObjectUpdate[];
	}
	interface IMatchUnderSetupVm {
		HostId: string;
		Id: string;
		Name: string;
		Slots: IPlayerSlot[];
	}
	interface IPlayerSlot {
		HasBot: boolean;
		HasPlayer: boolean;
		PlayerId: string;
		PlayerName: string;
	}
	interface IPosition {
		X: number;
		Y: number;
	}
	interface ISlotChangeMessage extends IAbstractIncomingClientMessage {
		ClearSlot: boolean;
		MatchId: string;
		SetBot: boolean;
		SlotIndex: number;
	}
	interface IStartMatchSetupRequest extends IAbstractIncomingClientMessage {
	}
	interface IStartQuickMatchRequest extends IAbstractIncomingClientMessage {
	}
	interface IUserConnectedMessage extends IAbstractIncomingClientMessage {
		UserId: string;
		Username: string;
	}
	interface IUserConnectionReply extends IAbstractOutgoingClientMessage {
		ClientId: string;
	}
	interface IUserListMessage extends IAbstractOutgoingClientMessage {
		Count: number;
		Usernames: string[];
	}
	interface IUserListRequest extends IAbstractIncomingClientMessage {
	}
declare module SplendidWar.Contracts.Messages {
	interface PlayerInputMessage extends IAbstractIncomingClientMessage {
	}
}


