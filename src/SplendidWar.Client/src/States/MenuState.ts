﻿/// <reference path="AbstractGameState.ts" />

// Main game menu
class MenuState extends AbstractGameState
{
    public static NAME = "state_menu";

    create()
    {
        super.create();
        
        this.Widgets.push(new MenuPanel(800, 600, new ServerCheckContent()));
    }
}