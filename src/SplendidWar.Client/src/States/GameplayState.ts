﻿/// <reference path="AbstractGameState.ts" />

// Main gameplay is handled in this state.
class GameplayState extends AbstractGameState
{
    public static NAME = "state_gameplay";
    
    init(msg: IMatchStartingMessage): void
    {
        this.world.width = msg.MapWidth;
        this.world.height = msg.MapHeight;
        this.camera.setBoundsToWorld();
    }

    create()
    {
        super.create();

        ServiceHolder.CommunicationService.RegisterMessageHandler(
            "MatchStepUpdateMessage",
            (msg: IMatchStepUpdateMessage) =>
            {
                this.HandleUpdateMessage(msg);
            });

        this.Widgets.push(new PlayerInputHandler());
        this.Widgets.push(new MiniMap());
    }

    update()
    {
        super.update();

        if (!ServiceHolder.CommunicationService.IsConnected())
        {
            this.game.state.start(MenuState.NAME);
        }
    }


    shutdown()
    {
        super.shutdown();

        ServiceHolder.CommunicationService.UnregisterMessageHandler("MatchStepUpdateMessage");
    }

    private HandleUpdateMessage(msg: IMatchStepUpdateMessage): void
    {
        for (var i = 0; i < msg.Updates.length; i++)
        {
            var update = msg.Updates[i];

            if (this.GameObjects[update.Id] == null)
            {
                if (!update.RemoveObject)
                {
                    this.CreateGameObject(update);
                }
            }
            else
            {
                if (update.RemoveObject)
                {
                    this.RemoveGameObject(this.GameObjects[update.Id]);
                }
                else
                {
                    this.GameObjects[update.Id].ApplyUpdate(update);
                }
            }
        }
    }

    private CreateGameObject(update: IGameObjectUpdate): void
    {
        this.GameObjects[update.Id] = new GameObject(update);
    }

    private RemoveGameObject(obj: GameObject): void
    {
        this.GameObjects[obj.Id] = null;
        obj.IsRemoved = true;
        obj.Destroy();
    }
}