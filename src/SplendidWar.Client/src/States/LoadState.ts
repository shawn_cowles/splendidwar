﻿// Handles loading of any assets
class LoadState extends Phaser.State
{
    public static NAME = "state_load";

    private loadBar: Phaser.Sprite;
    private titleSplash: Phaser.Sprite;

    preload()
    {
        super.preload();

        // TODO title splash
        //this.titleSplash = this.add.sprite(this.game.width / 2, this.game.height / 2, "title_splash");
        //this.titleSplash.anchor.set(0.5, 0.5);

        this.loadBar = this.add.sprite(this.game.width / 2 - 100, this.game.height / 2 - 10, 'load_sprite');
        this.load.setPreloadSprite(this.loadBar);

        // UI Sprites
        this.game.load.spritesheet("text_button", "images/ui/text_button.png", TextButton.WIDTH,
            TextButton.HEIGHT, 3, null, 1);
        this.game.load.spritesheet("sliced_window", "images/ui/sliced_window.png", 15, 15);

        var manifest = <IClientManifest>this.cache.getJSON("manifest");

        for (var i = 0; i < manifest.ShipImages.length; i++)
        {
            var spriteName = manifest.ShipImages[i];

            this.game.load.image(spriteName, "images/ships/" + spriteName + ".png");
        }
    }
    
    create()
    {
        // Fade progress bar and switch states
        var tween = this.add.tween(this.loadBar)
            .to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);

        tween.onComplete.add(() =>
        {
            // Load main menu
            this.game.state.start(MenuState.NAME, true, false);
        });
    }
}