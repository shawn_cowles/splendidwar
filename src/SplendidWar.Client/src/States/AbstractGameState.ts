﻿// Abstract base class for game states that have to handle GameObjects, UI and the like.
abstract class AbstractGameState extends Phaser.State
{
    public SpriteGroups: Phaser.Group[];
    public DeepBackgroundSprites: Phaser.Group;
    public BackgroundSprites: Phaser.Group;
    public MidgroundSprites: Phaser.Group;
    public ForegroundSprites: Phaser.Group;
    public OverlaySprites: Phaser.Group;

    public Graphics: Phaser.Graphics;
    public OverlayGraphics: Phaser.Graphics;

    public Widgets: AbstractUiWidget[];
    public GameObjects: GameObject[];

    public PointerInUi: boolean;

    create()
    {
        // Order is important, as it controls layering
        this.DeepBackgroundSprites = this.add.group();
        this.BackgroundSprites = this.add.group();
        this.Graphics = this.add.graphics(0, 0);
        this.MidgroundSprites = this.add.group();
        this.ForegroundSprites = this.add.group();
        this.OverlaySprites = this.add.group();
        this.OverlayGraphics = this.add.graphics(0, 0);

        this.SpriteGroups = [
            this.DeepBackgroundSprites,
            this.BackgroundSprites,
            this.MidgroundSprites,
            this.ForegroundSprites,
            this.OverlaySprites
        ];
        
        this.Widgets = [];
        this.GameObjects = [];
    }

    update()
    {
        this.Graphics.clear();
        this.OverlayGraphics.clear();

        var elapsedSeconds = this.time.elapsed / 1000;

        for (var i = 0; i < this.GameObjects.length; i++)
        {
            // may be null, due to the array being sparse
            if (this.GameObjects[i] != null)
            {
                this.GameObjects[i].Step(this, elapsedSeconds);
            }
        }

        this.PointerInUi = false;
        var pointerAsVector = new Vector2(
            this.input.activePointer.worldX,
            this.input.activePointer.worldY);

        for (var i = 0; i < this.Widgets.length; i++)
        {
            if (this.Widgets[i].PointIsInWidget(pointerAsVector))
            {
                this.PointerInUi = true;
            }
        }

        for (var i = 0; i < this.Widgets.length; i++)
        {
            this.Widgets[i].DoUpdate(this, elapsedSeconds);
        }

        for (var i = 0; i < this.GameObjects.length; i++)
        {
            // may be null, due to the array being sparse
            if (this.GameObjects[i] != null)
            {
                this.GameObjects[i].LateStep(this, elapsedSeconds);
            }
        }

        ServiceHolder.UpdateServices(elapsedSeconds);
    }
}