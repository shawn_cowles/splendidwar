﻿// Static class, holding services for use and sending to states.
class ServiceHolder
{
    public static CommunicationService: CommunicationService;
    public static PlayerDataService: PlayerDataService;

    public static Initialize(): void
    {
        this.CommunicationService = new CommunicationService();
        this.PlayerDataService = new PlayerDataService();

        this.CommunicationService.Initialize();
        this.PlayerDataService.Initialize();
    }

    public static UpdateServices(elapsedSeconds: number): void
    {
        this.CommunicationService.Update();
    }
}