﻿// Handles storing and persisting player data
class PlayerDataService
{
    private localStorageAvailable: boolean;

    private playerId: string;
    private playerName: string;
    
    constructor()
    {
    }

    public Initialize(): void
    {
        // !! to turn truthyness into a bool
        try
        {
            this.localStorageAvailable = !!localStorage;
        }
        catch (e)
        {
            this.localStorageAvailable = false;
        }

        if (!this.localStorageAvailable)
        {
            console.log("Local storage not available.");
        }

        if (this.localStorageAvailable && localStorage.getItem("player_id") != null)
        {
            this.playerId = localStorage.getItem("player_id");
        }
        else
        {
            this.playerId = "guest" + Math.random();

            if (this.localStorageAvailable)
            {
                localStorage.setItem("player_id", this.playerId);
            }
        }

        if (this.localStorageAvailable && localStorage.getItem("player_name") != null)
        {
            this.playerName = localStorage.getItem("player_name");
        }
        else
        {
            this.playerName = "";
        }
    }

    public PlayerId(): string
    {
        return this.playerId;
    }

    public GetPlayerName(): string
    {
        return this.playerName;
    }

    public SetPlayerName(newName: string): void
    {
        this.playerName = newName;

        if (this.localStorageAvailable)
        {
            localStorage.setItem("player_name", newName);
        }
    }
}
