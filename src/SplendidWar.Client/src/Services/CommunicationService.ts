﻿// Handles communication to and from the server
class CommunicationService
{
    private socket: WebSocket;

    private callbacks: { [messageName: string]: (msg: any) => void; }

    private isConnected: boolean;

    private sendQueue: IAbstractIncomingClientMessage[];

    public OwnClientId: string;

    constructor()
    {
        this.callbacks = {}
        this.isConnected = false;
        this.sendQueue = [];

        this.RegisterMessageHandler("UserConnectionReply",
            (msg: IUserConnectionReply) =>
            {
                this.OwnClientId = msg.ClientId;
            });
    }
    
    public IsConnected(): boolean
    {
        return this.isConnected;
    }
    
    public RegisterMessageHandler(messageType: string, handler: (msg: any) => void)
    {
        this.callbacks[messageType] = handler;
    }

    public UnregisterMessageHandler(messageType: string)
    {
        this.callbacks[messageType] = null;
    }

    public SendMessage(message: IAbstractIncomingClientMessage): void
    {
        if (this.isConnected)
        {
            this.socket.send(message.MessageName + "," + JSON.stringify(message));
        }
        else
        {
            // if the queue gets this long then the server is probably unreachable, stop adding messages
            if (this.sendQueue.length < 100)
            {
                this.sendQueue.push(message);
            }
        }
    }

    public TryConnect(): void
    {
        if (this.isConnected)
        {
            return;
        }

        this.socket = new WebSocket(SERVER_ROOT);

        this.socket.onopen = (evt: Event) =>
        {
            this.isConnected = true;
        };

        this.socket.onclose = (evt: Event) =>
        {
            this.isConnected = false;
        };

        this.socket.onmessage = (evt: MessageEvent) =>
        {
            this.MessageRecieved(<string>evt.data);
        };

        this.socket.onerror = (evt: Event) =>
        {
            if (this.isConnected)
            {
                console.log(evt);
            }
        };
    }

    public Initialize(): void
    {
        this.TryConnect();
    }

    public Update(): void
    {
        if (this.sendQueue.length > 0 && this.isConnected)
        {
            for (var i = 0; i < this.sendQueue.length; i++)
            {
                this.SendMessage(this.sendQueue[i]);
            }

            this.sendQueue = [];
        }
    }

    private MessageRecieved = (payload: string) =>
    {
        var commaIndex = payload.indexOf(',');

        var messageType = payload.substring(0, commaIndex);
        var json = payload.substring(commaIndex + 1);
        var message = JSON.parse(json);

        if (this.callbacks[messageType])
        {
            this.callbacks[messageType](message);
        }
    }
}