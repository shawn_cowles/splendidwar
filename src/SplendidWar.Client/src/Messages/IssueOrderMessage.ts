﻿class IssueOrderMessage implements IIssueOrderMessage
{
    public MessageName = "IssueOrderMessage";

    public OrderedObjectIds: number[];

    public Order: SplendidWar.Contracts.Data.OrderType;

    public TargetIds: number[];

    public TargetPositionX: number;

    public TargetPositionY: number;

    constructor(
        orderedObjectIds: number[],
        order: SplendidWar.Contracts.Data.OrderType,
        targetIds: number[],
        targetPosition: IPosition)
    {
        this.OrderedObjectIds = orderedObjectIds;
        this.Order = order;
        this.TargetIds = targetIds;
        this.TargetPositionX = targetPosition.X;
        this.TargetPositionY = targetPosition.Y;
    }
}