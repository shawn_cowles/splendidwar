﻿class UserConnectedMessage implements IUserConnectedMessage
{
    public MessageName = "UserConnectedMessage";

    public UserId: string;

    public Username: string;

    constructor(userId: string, username: string)
    {
        this.UserId = userId;
        this.Username = username;
    }
}