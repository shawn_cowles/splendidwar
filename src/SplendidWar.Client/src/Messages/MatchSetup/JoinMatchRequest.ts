﻿class JoinMatchRequest implements IJoinMatchRequest
{
    public MessageName = "JoinMatchRequest";

    public MatchId: string;

    constructor(matchId: string)
    {
        this.MatchId = matchId;
    }
}