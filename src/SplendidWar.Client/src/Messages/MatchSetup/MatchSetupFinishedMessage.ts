﻿class MatchSetupFinishedMessage implements IMatchSetupFinishedMessage
{
    public MessageName = "MatchSetupFinishedMessage";

    public MatchId: string;

    constructor(matchId: string)
    {
        this.MatchId = matchId;
    }
}