﻿class SlotChangeMessage implements ISlotChangeMessage
{
    public MessageName = "SlotChangeMessage";

    public MatchId: string;
    public SlotIndex: number;
    public ClearSlot: boolean;
    public SetBot: boolean;
}