﻿using ExtensibleBinaryTag;

namespace SplendidWar.Common.Data
{
    public class BarrelOffset : ITagRepresentable
    {
        public Vector2 Offset { get; set; }

        public double Facing { get; set; }

        public XBTag ToTag(string tagName)
        {
            var root = new XBTag(tagName);
            root.AddChild(new XBTag("offset_x", Offset.X));
            root.AddChild(new XBTag("offset_y", Offset.Y));
            root.AddChild(new XBTag("facing", Facing));
            return root;
        }

        public static BarrelOffset FromTag(XBTag root)
        {
            return new BarrelOffset
            {
                Offset = new Vector2(
                    root.GetChild("offset_x").GetPayload<double>(),
                    root.GetChild("offset_y").GetPayload<double>()),

                Facing = root.GetChild("facing").GetPayload<double>()
            };
        }
    }
}
