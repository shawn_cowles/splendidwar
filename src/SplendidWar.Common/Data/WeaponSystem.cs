﻿using System.Linq;
using ExtensibleBinaryTag;

namespace SplendidWar.Common.Data
{
    public class WeaponSystem : ITagRepresentable
    {
        public BarrelOffset[] BarrelOffsets { get; set; }

        public string Type { get; set; }
        public bool FixedMounting { get; set; }
        public int Shots { get; set; }
        public double ArmorPenetration { get; set; }
        public double Accuracy { get; set; }
        public double Cooldown { get; set; }
        public double Range { get; set; }
        
        public XBTag ToTag(string tagName)
        {
            var root = new XBTag(tagName);

            root.AddChild(XBTag.BuildCollection("barrels", BarrelOffsets));
            root.AddChild(new XBTag("type", Type));
            root.AddChild(new XBTag("fixed_mounting", FixedMounting));
            root.AddChild(new XBTag("shots", Shots));
            root.AddChild(new XBTag("armor_penetration", ArmorPenetration));
            root.AddChild(new XBTag("accuracy", Accuracy));
            root.AddChild(new XBTag("cooldown", Cooldown));
            root.AddChild(new XBTag("range", Range));

            return root;
        }

        public static WeaponSystem FromTag(XBTag root)
        {
            return new WeaponSystem
            {
                BarrelOffsets = XBTag.ReadCollection(root.GetChild("barrels"), BarrelOffset.FromTag)
                    .ToArray(),
                Type = root.GetChild("type").GetPayload<string>(),
                FixedMounting = root.GetChild("fixed_mounting").GetPayload<bool>(),
                Shots = root.GetChild("shots").GetPayload<int>(),
                ArmorPenetration = root.GetChild("armor_penetration").GetPayload<double>(),
                Accuracy = root.GetChild("accuracy").GetPayload<double>(),
                Cooldown = root.GetChild("cooldown").GetPayload<double>(),
                Range = root.GetChild("range").GetPayload<double>(),
            };
        }
    }
}
