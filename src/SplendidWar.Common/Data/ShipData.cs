﻿using System.Collections.Generic;
using System.Linq;
using ExtensibleBinaryTag;

namespace SplendidWar.Common.Data
{
    public class ShipData : ITagRepresentable
    {
        public string Name { get; set; }

        public int Armor { get; set; }
        public int Health { get; set; }

        public double Evasion { get; set; }
        public double Speed { get; set; }
        public double Acceleration { get; set; }
        public double TurnRate { get; set; }

        public string BaseSprite { get; set; }

        public List<Engine> Engines { get; private set; }

        public List<WeaponSystem> Weapons { get; private set; }

        public ShipData()
        {
            Engines = new List<Engine>();
            Weapons = new List<WeaponSystem>();
        }

        public XBTag ToTag(string tagName)
        {
            var root = new XBTag(tagName);
            root.AddChild(new XBTag("name", Name));
            root.AddChild(new XBTag("armor", Armor));
            root.AddChild(new XBTag("health", Health));
            root.AddChild(new XBTag("evasion", Evasion));
            root.AddChild(new XBTag("speed", Speed));
            root.AddChild(new XBTag("acceleration", Acceleration));
            root.AddChild(new XBTag("turn_rate", TurnRate));
            root.AddChild(new XBTag("base_sprite", BaseSprite));
            root.AddChild(XBTag.BuildCollection("engines", Engines));
            root.AddChild(XBTag.BuildCollection("weapons", Weapons));

            return root;
        }

        public static ShipData FromTag(XBTag root)
        {
            return new ShipData
            {
                Name = root.GetChild("name").GetPayload<string>(),
                Armor = root.GetChild("armor").GetPayload<int>(),
                Health = root.GetChild("health").GetPayload<int>(),
                Evasion = root.GetChild("evasion").GetPayload<double>(),
                Speed = root.GetChild("speed").GetPayload<double>(),
                Acceleration = root.GetChild("acceleration").GetPayload<double>(),
                TurnRate = root.GetChild("turn_rate").GetPayload<double>(),
                BaseSprite = root.GetChild("base_sprite").GetPayload<string>(),
                Engines = XBTag.ReadCollection(root.GetChild("engines"), Engine.FromTag).ToList(),
                Weapons = XBTag.ReadCollection(root.GetChild("weapons"), WeaponSystem.FromTag).ToList()
            };
        }
    }
}
