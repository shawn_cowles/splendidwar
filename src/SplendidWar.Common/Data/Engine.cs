﻿using ExtensibleBinaryTag;

namespace SplendidWar.Common.Data
{
    public class Engine : ITagRepresentable
    {
        public Vector2 Offset { get; set; }
        
        public double Width { get; set; }

        public XBTag ToTag(string tagName)
        {
            var root = new XBTag(tagName);
            root.AddChild(new XBTag("offset_x", Offset.X));
            root.AddChild(new XBTag("offset_y", Offset.Y));
            root.AddChild(new XBTag("width", Width));

            return root;
        }

        public static Engine FromTag(XBTag root)
        {
            return new Engine
            {
                Offset = new Vector2(
                    root.GetChild("offset_x").GetPayload<double>(),
                    root.GetChild("offset_y").GetPayload<double>()),

                Width = root.GetChild("width").GetPayload<double>()
            };
        }
    }
}
