﻿using System;
using SplendidWar.Contracts.Data;

namespace SplendidWar.Common.Data
{
    public struct Vector2
    {
        public static readonly Vector2 ZERO = new Vector2(0, 0);
        public static readonly Vector2 UP = new Vector2(0, -1);
        public static readonly Vector2 DOWN = new Vector2(0, 1);
        public static readonly Vector2 RIGHT = new Vector2(1, 0);
        public static readonly Vector2 LEFT = new Vector2(-1, 0);

        public readonly double X;
        public readonly double Y;

        public Vector2(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double Magnitude
        {
            get { return Math.Sqrt(X * X + Y * Y); }
        }

        public double MagnitudeSqared
        {
            get { return X * X + Y * Y; }
        }

        public double Direction
        {
            get { return Math.Atan2(Y, X); }
        }

        public Vector2 Normalized
        {
            get { return FromPolar(1, Direction); }
        }

        public double Dot(Vector2 other)
        {
            return X * other.X + Y * other.Y;
        }
        
        public Vector2 Scale(double factor)
        {
            return new Vector2(X * factor, Y * factor);
        }
        
        public double DistanceTo(Vector2 other)
        {
            var dx = X - other.X;
            var dy = Y - other.Y;

            return Math.Sqrt(dx * dx + dy * dy);
        }

        public double DirectionTo(Vector2 other)
        {
            var dx = other.X - X;
            var dy = other.Y - Y;

            return Math.Atan2(dy, dx);
        }

        public double BearingTo(double ownFacing, Vector2 other)
        {
            var direction = DirectionTo(other);

            var raw = direction - ownFacing;

            if (raw > Math.PI)
            {
                return raw - 2 * Math.PI;
            }

            if (raw < -Math.PI)
            {
                return raw + 2 * Math.PI;
            }

            return raw;
        }

        public Position ToPosition()
        {
            return new Position
            {
                X = X,
                Y = Y
            };
        }

        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

        public static Vector2 operator -(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }

        public static Vector2 FromPolar(double magnitude, double direction)
        {
            var x = magnitude * Math.Cos(direction);
            var y = magnitude * Math.Sin(direction);

            return new Vector2(x, y);
        }

        public static Vector2 FromPosition(Position position)
        {
            return new Vector2(position.X, position.Y);
        }
    }
}
