﻿using System;

namespace SplendidWar.Server.Interfaces
{
    public interface ILoggerProvider
    {
        ILogger GetLoggerFor(Type t);
    }
}
