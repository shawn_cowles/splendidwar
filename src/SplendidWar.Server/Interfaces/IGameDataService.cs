﻿using SplendidWar.Common.Data;

namespace SplendidWar.Server.Interfaces
{
    public interface IGameDataService
    {
        MapData GetTestMap();

        ShipData GetDataForShip(string shipName);
    }
}
