﻿using SuperSocket.SocketBase.Config;

namespace SplendidWar.Server.Interfaces
{
    public interface ISettingsProvider
    {
        string GetDataDirectory { get; }
        
        IConfigurationSource GetWebsocketConfiguration();
    }
}
