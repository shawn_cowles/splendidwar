﻿namespace SplendidWar.Server.Interfaces
{
    public interface IInitializableService
    {
        void Initialize();
    }
}
