﻿namespace SplendidWar.Server
{
    public class HexColor
    {
        public static readonly int WHITE = 0xFFFFFF;

        public static readonly int RED = 0xFF0000;

        public static readonly int BLUE = 0x0000FF;

        public static readonly int GREEN = 0x00FF00;

        public static readonly int CYAN = 0x00FFFF;

        public static readonly int YELLOW = 0xFFFF00;

        public static readonly int PURPLE = 0xFF00FF;

        public static readonly int ORANGE = 0xFF8000;

        public static readonly int WINDOW_BORDER = 0x3e81ff;

        public static readonly int WINDOW_BACKGROUND = 0x2e4080;
    }
}
