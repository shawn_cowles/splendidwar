﻿using SplendidWar.Contracts.Messages;
using SplendidWar.Server.Data;

namespace SplendidWar.Server.Messages
{
    internal class StartMatchMessage : AbstractMessage
    {
        public MatchUnderSetup MatchSetup { get; }

        public StartMatchMessage(MatchUnderSetup matchSetup)
        {
            MatchSetup = matchSetup;
        }
    }
}