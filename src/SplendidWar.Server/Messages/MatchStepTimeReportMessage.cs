﻿using System;
using SplendidWar.Contracts.Messages;

namespace SplendidWar.Server.Messages
{
    internal class MatchStepTimeReportMessage : AbstractMessage
    {
        public TimeSpan Elapsed { get; }

        public MatchStepTimeReportMessage(TimeSpan elapsed)
        {
            Elapsed = elapsed;
        }
    }
}