﻿using SplendidWar.Contracts.Messages;

namespace SplendidWar.Server.Messages
{
    public class UserJoinedMatchMessage : AbstractMessage
    {
        public string ClientId { get; }

        public UserJoinedMatchMessage(string clientId)
        {
            ClientId = clientId;
        }
    }
}
