﻿using SplendidWar.Contracts.Messages;

namespace SplendidWar.Server.Messages
{
    internal class BotRequiredMessage : AbstractMessage
    {
        public string BotId { get; }

        public BotRequiredMessage(string botId)
        {
            BotId = botId;
        }
    }
}