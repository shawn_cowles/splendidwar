﻿using SplendidWar.Contracts.Messages;

namespace SplendidWar.Server.Messages
{
    public class UserLeftMatchMessage : AbstractMessage
    {
        public string ClientId { get; }

        public UserLeftMatchMessage(string clientId)
        {
            ClientId = clientId;
        }
    }
}
