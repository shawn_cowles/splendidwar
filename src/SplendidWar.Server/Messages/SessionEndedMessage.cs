﻿using SplendidWar.Contracts.Messages;
using SplendidWar.Server.Data;

namespace SplendidWar.Server.Messages
{
    public class SessionEndedMessage : AbstractMessage
    {
        public UserSession Session { get; private set; }
        
        public SessionEndedMessage(UserSession session)
        {
            Session = session;
        }
    }
}
