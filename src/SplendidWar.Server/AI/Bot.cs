﻿namespace SplendidWar.Server.AI
{
    public class Bot
    {
        public string ClientId { get; }

        public Bot(string clientId)
        {
            ClientId = clientId;
        }
    }
}
