﻿namespace SplendidWar.Server.Data
{
    public class Player
    {
        public string ClientId { get; }

        public Player(string clientId)
        {
            ClientId = clientId;
        }
    }
}
