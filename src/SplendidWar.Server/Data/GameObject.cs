﻿using System.Collections.Generic;
using System.Linq;
using SplendidWar.Common.Data;
using SplendidWar.Contracts.Data;
using SplendidWar.Server.Data.Components;

namespace SplendidWar.Server.Data
{
    public class GameObject
    {
        private int _nextComponentId;

        private RunningMatch _match;
        public int Id { get; }
        public Vector2 Position { get; set; }
        public double Facing { get; set; }
        public bool ShouldBeRemoved { get; set; }

        private List<AbstractComponent> _components;
        private HashSet<int> _pendingAddIds;
        private HashSet<int> _pendingRemoveIds;
        private Dictionary<AbstractComponent, int> _idForComponent;
        private double _timestamp;

        internal GameObject(RunningMatch match, int id, Vector2 position, double facing)
        {
            _match = match;
            Id = id;
            Position = position;
            Facing = facing;
            ShouldBeRemoved = false;
            _components = new List<AbstractComponent>();
            _pendingAddIds = new HashSet<int>();
            _pendingRemoveIds = new HashSet<int>();
            _idForComponent = new Dictionary<AbstractComponent, int>();
            _nextComponentId = 0;
            _timestamp = 0;
        }

        public void Add(AbstractComponent component)
        {
            _idForComponent.Add(component, _nextComponentId);
            _pendingAddIds.Add(_nextComponentId);
            _nextComponentId += 1;
            _components.Add(component);
            component.Parent = this;
            CacheComponents();
        }

        public T GetComponent<T>() where T : AbstractComponent
        {
            return _components
                .OfType<T>()
                .FirstOrDefault();
        }

        public IEnumerable<T> GetComponents<T>() where T : AbstractComponent
        {
            return _components
                .OfType<T>();
        }

        public bool HasComponent<T>() where T : AbstractComponent
        {
            return _components
                    .OfType<T>()
                    .Any();
        }

        public void RemoveComponentsOfType<T>() where T : AbstractComponent
        {
            for (var i = 0; i < _components.Count; i++)
            {
                if (_components[i] is T)
                {
                    _pendingRemoveIds.Add(_idForComponent[_components[i]]);
                    _idForComponent.Remove(_components[i]);
                    _components[i].OnRemoved();
                    _components.RemoveAt(i);
                    i -= 1;
                }
            }

            CacheComponents();
        }

        public void RemoveComponent(RunningMatch match, AbstractComponent component)
        {
            _pendingRemoveIds.Add(_idForComponent[component]);
            _idForComponent.Remove(component);
            component.OnRemoved();
            _components.Remove(component);
            CacheComponents();
        }

        public void Step(RunningMatch match, double elapsedSeconds)
        {
            _timestamp += elapsedSeconds;

            foreach (var component in _components)
            {
                component.Step(match, elapsedSeconds);
            }
        }

        public void OnRemoved()
        {
            foreach (var component in _components)
            {
                component.OnRemoved();
            }
        }

        public void OnCollided(RunningMatch match, GameObject other)
        {
            if (other != this)
            {
                foreach (var component in _components)
                {
                    component.OnCollided(match, other);
                }
            }
        }

        public void BroadcastMessage(RunningMatch match, string message)
        {
            foreach (var component in _components)
            {
                component.OnMessage(match, message);
            }
        }

        private void CacheComponents()
        {
        }


        public GameObjectUpdate BuildUpdate()
        {
            var partialUpdates = _components
                .Where(c => c.IsClientVisible)
                .Where(c => !_pendingAddIds.Contains(_idForComponent[c]))
                .Where(c => !_pendingRemoveIds.Contains(_idForComponent[c]))
                .Where(c => c.HasPartialUpdates)
                .Select(c => c.BuildPartialUpdate(_idForComponent[c]))
                .ToArray();

            var addUpdates = _components
                .Where(c => c.IsClientVisible)
                .Where(c => _pendingAddIds.Contains(_idForComponent[c]))
                .Where(c => !_pendingRemoveIds.Contains(_idForComponent[c]))
                .Select(c => c.BuildFullUpdate(_idForComponent[c]))
                .ToArray();

            var removeUpdates = _components
                .Where(c => c.IsClientVisible)
                .Where(c => _pendingRemoveIds.Contains(_idForComponent[c]))
                .Select(c => ComponentUpdate.BuildRemoveUpdate(_idForComponent[c]))
                .ToArray();

            _pendingAddIds.Clear();
            _pendingRemoveIds.Clear();

            return new GameObjectUpdate(Id)
            {
                Timestamp = _timestamp,
                Position = Position.ToPosition(),
                Facing = Facing,
                ComponentUpdates = partialUpdates
                    .Concat(addUpdates)
                    .Concat(removeUpdates)
                    .ToArray()
            };
        }
    }
}
