﻿using System;
using System.Collections.Generic;
using System.Linq;
using SplendidWar.Common.Data;
using SplendidWar.Contracts.Data;
using SplendidWar.Contracts.Messages;
using SplendidWar.Server.Data.Components;
using SplendidWar.Server.Data.Orders;

namespace SplendidWar.Server.Data
{
    public class RunningMatch
    {
        public string Id { get; }

        private int _nextObjectId;

        private readonly MapData _mapData;
        private DateTime _lastUpdate;

        private List<GameObject> _gameObjects;
        private List<GameObject> _addQueue;

        private Dictionary<int, AbstractOrder> _pendingOrders;

        public Player[] Players { get; }

        private List<PlayerInputMessage> _inputQueue;
        
        public RunningMatch(string id, MapData mapData, Player[] players)
        {
            Id = id;
            _mapData = mapData;
            Players = players;
            _lastUpdate = DateTime.UtcNow;
            _gameObjects = new List<GameObject>();
            _addQueue = new List<GameObject>();
            _inputQueue = new List<PlayerInputMessage>();
            _pendingOrders = new Dictionary<int, AbstractOrder>();
        }

        public GameObjectUpdate[] Step()
        {
            var now = DateTime.UtcNow;
            var elapsedSeconds = (now - _lastUpdate).TotalSeconds;
            _lastUpdate = now;

            ProcessInput();

            var updates = StepGameObjects(elapsedSeconds);

            _pendingOrders.Clear();

            return updates;
        }

        public GameObject CreateGameObject(Vector2 position, double facing)
        {
            var obj = new GameObject(this, _nextObjectId, position, facing);

            _nextObjectId += 1;

            _addQueue.Add(obj);

            return obj;
        }

        public void EnqueueInput(PlayerInputMessage message)
        {
            lock(_inputQueue)
            {
                _inputQueue.Add(message);
            }
        }

        private void ProcessInput()
        {
            PlayerInputMessage[] queue;
            lock (_inputQueue)
            {
                queue = _inputQueue.ToArray();
                _inputQueue.Clear();
            }

            foreach(var message in queue)
            {
                if(message is IssueOrderMessage)
                {
                    IssueOrder(message as IssueOrderMessage);
                }
            }
        }
        
        private GameObjectUpdate[] StepGameObjects(double elapsedSeconds)
        {
            foreach (var gameObject in _gameObjects)
            {
                if(_pendingOrders.ContainsKey(gameObject.Id))
                {
                    gameObject.GetComponent<OrderHandler>()?.SetOrder(_pendingOrders[gameObject.Id]);
                }

                gameObject.Step(this, elapsedSeconds);
            }

            _gameObjects.AddRange(_addQueue);

            var updates = _gameObjects
                .Where(go => !go.ShouldBeRemoved)
                .Select(go => go.BuildUpdate())
                .Concat(_gameObjects
                    .Where(go => go.ShouldBeRemoved)
                    .Select(go => GameObjectUpdate.RemoveUpdate(go.Id)))
                .ToArray();

            _addQueue.Clear();

            _gameObjects.RemoveAll(go => go.ShouldBeRemoved);

            return updates;
        }

        private void IssueOrder(IssueOrderMessage issueOrderMessage)
        {
            // Add to lookup, so that in regular processing game objects can grab orders as 
            // they process, order objects, directly used in AI behavior
            foreach(var id in issueOrderMessage.OrderedObjectIds)
            {
                var order = AbstractOrder.BuildOrder(issueOrderMessage);

                if(!_pendingOrders.ContainsKey(id))
                {
                    _pendingOrders.Add(id, order);
                }
                else
                {
                    _pendingOrders[id] = order;
                }
            }
        }
    }
}
