﻿using System;

namespace SplendidWar.Server.Data
{
    public class UserSession
    {
        public string ClientId { get; set; }

        public string UserId { get; set; }
        
        public string Username { get; set; }

        public DateTime StartDate { get; set; }
        
        public DateTime EndDate { get; set; }

        public bool InMatch { get; set; }
        
        public UserSession()
        {
        }
    }
}
