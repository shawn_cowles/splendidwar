﻿using System;
using SplendidWar.Common.Data;
using SplendidWar.Server.Data.Components;

namespace SplendidWar.Server.Data.Orders
{
    public class MoveOrder : AbstractOrder
    {
        private Vector2 _targetPoint;

        public MoveOrder(Vector2 targetPoint)
        {
            _targetPoint = targetPoint;
        }

        public override string Name
        {
            get { return "Moving"; }
        }

        public override void Step(
            GameObject parent, 
            InputSource inputSource,
            RunningMatch match, 
            double elapsedSeconds)
        {
            var toTarget = _targetPoint - parent.Position;
            
            inputSource.DesiredFacing = toTarget.Direction;

            if(parent.Position.DistanceTo(_targetPoint) > 50)
            {
                var desiredSpeed = Math.Min(inputSource.TopSpeed, toTarget.Magnitude / 4);
                inputSource.ThrottlePercent = desiredSpeed / inputSource.TopSpeed;
            }
            else
            {
                inputSource.ThrottlePercent = 0;
            }
        }
    }
}