﻿using System;
using SplendidWar.Common.Data;
using SplendidWar.Contracts.Data;
using SplendidWar.Contracts.Messages;
using SplendidWar.Server.Data.Components;

namespace SplendidWar.Server.Data.Orders
{
    public abstract class AbstractOrder
    {
        public abstract string Name { get; }

        public abstract void Step(
            GameObject parent,
            InputSource inputSource,
            RunningMatch match,
            double elapsedSeconds);

        public static AbstractOrder BuildOrder(IssueOrderMessage msg)
        {
            switch(msg.Order)
            {
                case OrderType.Move:
                    return new MoveOrder(new Vector2(msg.TargetPositionX, msg.TargetPositionY));
                default:
                    throw new Exception("Unknown order type: " + msg.Order);
            }
        }
    }
}
