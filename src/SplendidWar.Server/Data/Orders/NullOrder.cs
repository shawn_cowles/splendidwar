﻿using SplendidWar.Server.Data.Components;

namespace SplendidWar.Server.Data.Orders
{
    public class NullOrder : AbstractOrder
    {
        public override string Name
        {
            get { return "Idle"; }
        }

        public override void Step(
            GameObject parent,
            InputSource inputSource,
            RunningMatch match,
            double elapsedSeconds)
        {
            inputSource.DesiredFacing = parent.Facing;
            inputSource.ThrottlePercent = 0;
        }
    }
}
