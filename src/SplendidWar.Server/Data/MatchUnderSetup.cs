﻿using System;
using System.Linq;
using SplendidWar.Contracts.Data;

namespace SplendidWar.Server.Data
{
    public class MatchUnderSetup
    {
        public string Id { get; }

        public string Name { get; }

        public string HostId { get; }

        public PlayerSlot[] Slots { get; }

        public MatchUnderSetup(string hostId, string name)
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
            HostId = hostId;
            Slots = new PlayerSlot[6]
            {
                new PlayerSlot(),
                new PlayerSlot(),
                new PlayerSlot(),
                new PlayerSlot(),
                new PlayerSlot(),
                new PlayerSlot()
            };
        }

        public string[] PlayersPresent
        {
            get
            {
                return Slots
                        .Where(s => s.HasPlayer)
                        .Select(s => s.PlayerId)
                        .ToArray();
            }
        }

        public bool HasCapacity
        {
            get
            {
                return Slots
                    .Where(s => !s.HasPlayer)
                    .Where(s => !s.HasBot)
                    .Any();
            }
        }

        public MatchUnderSetupVm BuildSummary()
        {
            return new MatchUnderSetupVm
            {
                Id = Id,
                HostId = HostId,
                Name = Name,
                Slots = Slots
            };
        }
    }
}
