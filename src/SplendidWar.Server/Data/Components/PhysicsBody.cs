﻿using SplendidWar.Common.Data;

namespace SplendidWar.Server.Data.Components
{
    public class PhysicsBody : AbstractComponent
    {
        public Vector2 Velocity { get; set; }

        public PhysicsBody()
        {
            Velocity = Vector2.ZERO;
        }

        public override void Step(RunningMatch match, double elapsedSeconds)
        {
            base.Step(match, elapsedSeconds);

            Parent.Position += Velocity.Scale(elapsedSeconds);
        }
    }
}
