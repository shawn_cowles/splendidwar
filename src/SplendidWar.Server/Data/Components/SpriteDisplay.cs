﻿using SplendidWar.Contracts.Data;

namespace SplendidWar.Server.Data.Components
{
    public class SpriteDisplay : AbstractClientVisibleComponent
    {
        public static int LAYER_DEEP_BACKGROUND = 0;
        public static int LAYER_BACKGROUND = 1;
        public static int LAYER_MIDGROUND = 2;
        public static int LAYER_FOREGROUND = 3;
        public static int LAYER_OVERLAY = 4;
        
        private string _spriteName;
        private int _tint;
        private double _scale;
        private int _layer;

        public SpriteDisplay(string spriteName, int tint, double scale, int layer)
        {
            _spriteName = spriteName;
            _tint = tint;
            _scale = scale;
            _layer = layer;
        }

        public override bool HasPartialUpdates
        {
            get { return false; }
        }
        
        public override ComponentUpdate BuildFullUpdate(int componentId)
        {
            var update = new ComponentUpdate(componentId, GetType().Name);
            update.Data.Add(_spriteName);
            update.Data.Add(_tint);
            update.Data.Add(_scale);
            update.Data.Add(_layer);
            return update;
        }
    }
}
