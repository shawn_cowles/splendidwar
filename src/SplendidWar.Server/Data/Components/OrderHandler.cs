﻿using SplendidWar.Contracts.Data;
using SplendidWar.Server.Data.Orders;

namespace SplendidWar.Server.Data.Components
{
    public class OrderHandler : AbstractClientVisibleComponent
    {
        private AbstractOrder _currentOrder;
        private InputSource _inputSource;

        public OrderHandler()
        {
            _currentOrder = new NullOrder();
        }

        protected override void Initialize(RunningMatch match)
        {
            base.Initialize(match);

            _inputSource = Parent.GetComponent<InputSource>();
        }

        public override bool HasPartialUpdates
        {
            get { return true; }
        }

        public override void Step(RunningMatch match, double elapsedSeconds)
        {
            base.Step(match, elapsedSeconds);

            _currentOrder.Step(Parent, _inputSource, match, elapsedSeconds);
        }

        public override ComponentUpdate BuildFullUpdate(int componentId)
        {
            var update = new ComponentUpdate(componentId, GetType().Name);
            update.Data.Add(_currentOrder.Name);
            return update;
        }

        public override ComponentUpdate BuildPartialUpdate(int componentId)
        {
            return BuildFullUpdate(componentId);
        }

        public void SetOrder(AbstractOrder newOrder)
        {
            _currentOrder = newOrder;
        }
    }
}
