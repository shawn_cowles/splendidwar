﻿using System;
using SplendidWar.Common.Data;

namespace SplendidWar.Server.Data.Components
{
    public class Thrusters : AbstractComponent
    {
        private InputSource _inputSource;
        private PhysicsBody _physicsBody;

        private double _acceleration;
        private double _topSpeed;

        public Thrusters(double topSpeed, double acceleration)
        {
            _acceleration = acceleration;
            _topSpeed = topSpeed;
        }
        protected override void Initialize(RunningMatch match)
        {
            base.Initialize(match);

            _inputSource = Parent.GetComponent<InputSource>();
            _physicsBody = Parent.GetComponent<PhysicsBody>();
        }

        public override void Step(RunningMatch match, double elapsedSeconds)
        {
            base.Step(match, elapsedSeconds);

            var speed = _physicsBody.Velocity.Magnitude;

            // Apply turn loss
            var directionChange = Util.RadiansBetween(
                Parent.Facing,
                _physicsBody.Velocity.Direction);
            var turnLoss = (directionChange / Math.PI) * 1;
            speed = speed - speed * turnLoss;

            // Determine Acceleration
            var targetSpeed = _inputSource.ThrottlePercent * _topSpeed;
            var acceleration = _acceleration * elapsedSeconds;


            // Apply acceleration
            if (targetSpeed > speed)
            {
                speed = Math.Min(speed + acceleration, targetSpeed);
            }
            else
            {
                speed = Math.Max(speed - acceleration, targetSpeed);
            }

            _physicsBody.Velocity = Vector2.FromPolar(speed, Parent.Facing);
        }
    }
}
