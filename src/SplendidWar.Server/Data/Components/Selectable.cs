﻿using SplendidWar.Contracts.Data;

namespace SplendidWar.Server.Data.Components
{
    public class Selectable : AbstractClientVisibleComponent
    {
        private string _objectName;
        
        public Selectable(string objectName)
        {
            _objectName = objectName;
        }

        public override bool HasPartialUpdates
        {
            get { return false; }
        }

        public override ComponentUpdate BuildFullUpdate(int componentId)
        {
            var update = new ComponentUpdate(componentId, GetType().Name);
            update.Data.Add(_objectName);
            return update;
        }
    }
}
