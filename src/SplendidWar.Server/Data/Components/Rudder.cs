﻿namespace SplendidWar.Server.Data.Components
{
    public class Rudder : AbstractComponent
    {
        private InputSource _inputSource;

        private double _turnRate;

        public Rudder(double turnRate)
        {
            _turnRate = turnRate;
        }
        protected override void Initialize(RunningMatch match)
        {
            base.Initialize(match);

            _inputSource = Parent.GetComponent<InputSource>();
        }

        public override void Step(RunningMatch match, double elapsedSeconds)
        {
            base.Step(match, elapsedSeconds);

            var maxTurn = _turnRate * elapsedSeconds;
            var facing = Util.CorrectAngle(Parent.Facing);
            var delta = _inputSource.DesiredFacing - facing;
            var correctedDelta = Util.CorrectAngle(delta);
            var turn = Util.Clamp(correctedDelta, -maxTurn, maxTurn);
            Parent.Facing = Util.CorrectAngle(facing + turn);
        }
    }
}
