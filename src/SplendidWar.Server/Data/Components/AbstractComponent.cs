﻿using System;
using SplendidWar.Contracts.Data;

namespace SplendidWar.Server.Data.Components
{
    public abstract class AbstractComponent
    {
        public GameObject Parent { get; set; }

        private bool _isInitialized;

        protected AbstractComponent()
        {
            _isInitialized = false;
        }

        public virtual bool IsClientVisible
        {
            get { return false; }
        }

        public virtual bool HasPartialUpdates
        {
            get { return false; }
        }

        protected virtual void Initialize(RunningMatch match)
        {
        }

        public virtual void Step(RunningMatch match, double elapsedSeconds)
        {
            if (!_isInitialized)
            {
                Initialize(match);
                _isInitialized = true;
            }
        }

        protected virtual void Uninitialize()
        {
        }

        public void OnRemoved()
        {
            if (_isInitialized)
            {
                Uninitialize();
            }
        }

        public void OnCollided(RunningMatch match, GameObject other)
        {
        }

        public virtual void OnMessage(RunningMatch match, string message)
        {
        }

        public virtual ComponentUpdate BuildPartialUpdate(int componentId)
        {
            throw new NotImplementedException();
        }

        public virtual ComponentUpdate BuildFullUpdate(int componentId)
        {
            throw new NotImplementedException();
        }
    }
}
