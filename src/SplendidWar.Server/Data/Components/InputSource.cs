﻿namespace SplendidWar.Server.Data.Components
{
    public class InputSource : AbstractComponent
    {
        public double TopSpeed { get; }

        public double DesiredFacing { get; set; }

        public double ThrottlePercent { get; set; }

        public InputSource(double topSpeed)
        {
            TopSpeed = topSpeed;
        }
    }
}
