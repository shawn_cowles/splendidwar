﻿using SplendidWar.Contracts.Data;

namespace SplendidWar.Server.Data.Components
{
    public abstract class AbstractClientVisibleComponent : AbstractComponent
    {
        public override abstract bool HasPartialUpdates { get; }
        
        public override bool IsClientVisible
        {
            get { return true; }
        }

        public override abstract ComponentUpdate BuildFullUpdate(int componentId);
    }
}
