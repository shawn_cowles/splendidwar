﻿using SplendidWar.Contracts.Data;

namespace SplendidWar.Server.Data.Components
{
    public class Ownership : AbstractClientVisibleComponent
    {
        public string OwnerId { get; set; }

        public Ownership(string ownerId)
        {
            OwnerId = ownerId;
        }

        public override bool HasPartialUpdates
        {
            get { return false; }
        }

        public override ComponentUpdate BuildFullUpdate(int componentId)
        {
            var update = new ComponentUpdate(componentId, GetType().Name);
            update.Data.Add(OwnerId);
            return update;
        }
    }
}
