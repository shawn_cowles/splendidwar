﻿using System;

namespace SplendidWar.Server
{
    public class Util
    {
        // Bring an angle to the range of PI to -PI, facilitates angle calculations
        public static double CorrectAngle(double inputAngle)
        {
            while (inputAngle > Math.PI)
            {
                inputAngle -= 2 * Math.PI;
            }

            while (inputAngle < -Math.PI)
            {
                inputAngle += 2 * Math.PI;
            }

            return inputAngle;
        }

        // Clamp a number to a range
        public static double Clamp(double input, double min, double max)
        {
            if (input < min)
            {
                return min;
            }

            if (input > max)
            {
                return max;
            }

            return input;
        }

        // Determine the radians between two angles
        public static double RadiansBetween(double facingA, double facingB)
        {
            return Math.Abs(CorrectAngle(facingB - facingA));
        }
    }
}
