﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using SplendidWar.Common.Data;
using SplendidWar.Contracts.Messages;
using SplendidWar.Server.Data;
using SplendidWar.Server.Data.Components;
using SplendidWar.Server.Interfaces;
using SplendidWar.Server.Messages;

namespace SplendidWar.Server.Services
{
    public class MatchTracker : AbstractBusService
    {
        private const int MATCH_STEP_TICK = 30;

        private readonly Dictionary<string, RunningMatch> _runningMatches;
        private readonly Dictionary<string, Timer> _matchTimers;
        private readonly Dictionary<string, string> _clientIdToMatchId;
        private readonly IGameDataService _gameData;

        public MatchTracker(ILoggerProvider loggerProvider, IGameDataService gameData)
            : base(loggerProvider.GetLoggerFor(typeof(MatchTracker)))
        {
            _gameData = gameData;
            _runningMatches = new Dictionary<string, RunningMatch>();
            _matchTimers = new Dictionary<string, Timer>();
            _clientIdToMatchId = new Dictionary<string, string>();

            SetMessageHandler<StartQuickMatchRequest>(HandleStartQuickMatchRequest);
            SetMessageHandler<StartMatchMessage>(HandleStartMatchMessage);
            SetMessageHandler<UserDisconnectedMessage>(HandleUserDisconnected);
            SetMessageHandler<PlayerInputMessage>(HandlePlayerInput);
        }
        
        public override void Stop()
        {
            base.Stop();

            foreach (var timer in _matchTimers.Values)
            {
                timer.Dispose();
            }
        }

        private void HandleStartQuickMatchRequest(StartQuickMatchRequest message)
        {
            try
            {
                if (_clientIdToMatchId.ContainsKey(message.ClientId))
                {
                    Logger.Warn("Received StartGameRequest for user already in a match.");
                }
                else
                {
                    var matchSetup = new MatchUnderSetup(message.ClientId, "Quick Match");
                    matchSetup.Slots[0].SetPlayer(message.ClientId, "Guest");
                    matchSetup.Slots[1].SetBot();

                    StartMatch(matchSetup);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void HandleStartMatchMessage(StartMatchMessage message)
        {
            try
            {
                if (_runningMatches.ContainsKey(message.MatchSetup.Id))
                {
                    Logger.Warn("Received StartGameRequest for match already in progress.");
                }
                else
                {
                    StartMatch(message.MatchSetup);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
        
        private void HandleUserDisconnected(UserDisconnectedMessage message)
        {
            // Expect user disconnects for users not in a match,
            if (!_clientIdToMatchId.ContainsKey(message.ClientId))
            {
                return;
            }

            // Stop the match
            var matchId = _clientIdToMatchId[message.ClientId];
            var match = _runningMatches[matchId];

            _clientIdToMatchId.Remove(message.ClientId);
            _matchTimers[matchId].Dispose();
            _matchTimers.Remove(matchId);
            _runningMatches.Remove(matchId);

            // TODO SendMessage(new MatchEndedMessage(matchId, players in the match))
        }

        private void HandlePlayerInput(PlayerInputMessage message)
        {
            if (!_clientIdToMatchId.ContainsKey(message.ClientId))
            {
                Logger.Warn("Received PlayerInputMessage for user not in a match.");
            }
            else
            {
                var match = _runningMatches[_clientIdToMatchId[message.ClientId]];

                match.EnqueueInput(message);
            }
        }

        private void StartMatch(MatchUnderSetup matchSetup)
        {
            var players = new List<Player>();

            foreach (var slot in matchSetup.Slots)
            {
                if (slot.HasPlayer)
                {
                    players.Add(new Player(slot.PlayerId));
                    _clientIdToMatchId.Add(slot.PlayerId, matchSetup.Id);
                }
                if (slot.HasBot)
                {
                    var botId = Guid.NewGuid().ToString();
                    players.Add(new Player(botId));
                    SendMessage(new BotRequiredMessage(botId));
                    _clientIdToMatchId.Add(botId, matchSetup.Id);
                }
            }

            var map = _gameData.GetTestMap();

            var match = new RunningMatch(
                matchSetup.Id,
                map,
                players.ToArray()
                // new DeathmatchMission()
                );

            _runningMatches.Add(match.Id, match);
            _matchTimers.Add(match.Id, new Timer(StepGame, match, MATCH_STEP_TICK, MATCH_STEP_TICK));

            var playerClientIds = matchSetup.Slots
                .Where(s => s.HasPlayer)
                .Select(s => s.PlayerId)
                .ToArray();

            SendMessage(new MatchStartingMessage(playerClientIds, match.Id, map.Width, map.Height));

            var x = 100;
            foreach (var player in match.Players)
            {
                for(var i = 0; i < 10;i++)
                {
                    BuildShip(match, new Vector2(x, 50 * i), 0, "B_Corvette", player);
                }

                x += 200;
            }
        }

        private void BuildShip(
            RunningMatch match, 
            Vector2 position, 
            int facing, 
            string className, 
            Player owner)
        {
            var shipData = _gameData.GetDataForShip(className);

            var obj = match.CreateGameObject(position, facing);
            obj.Add(new SpriteDisplay(shipData.BaseSprite, HexColor.WHITE, 1, SpriteDisplay.LAYER_MIDGROUND));
            obj.Add(new Ownership(owner.ClientId));
            obj.Add(new Selectable("Test Ship"));
            obj.Add(new InputSource(shipData.Speed));
            obj.Add(new PhysicsBody());
            obj.Add(new Rudder(shipData.TurnRate));
            obj.Add(new Thrusters(shipData.Speed, shipData.Acceleration));
            obj.Add(new OrderHandler());
        }

        private void StepGame(object state)
        {
            try
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                var match = (RunningMatch)state;

                var updates = match.Step();

                SendMessage(new MatchStepUpdateMessage(
                    match.Players.Select(p => p.ClientId).ToArray(),
                     updates));


                stopwatch.Stop();
                SendMessage(new MatchStepTimeReportMessage(stopwatch.Elapsed));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}
