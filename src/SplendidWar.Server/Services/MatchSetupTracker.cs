﻿using System;
using System.Collections.Generic;
using System.Linq;
using SplendidWar.Contracts.Messages;
using SplendidWar.Contracts.Messages.MatchSetup;
using SplendidWar.Server.Data;
using SplendidWar.Server.Interfaces;
using SplendidWar.Server.Messages;

namespace SplendidWar.Server.Services
{
    public class MatchSetupTracker : AbstractBusService
    {
        private readonly Dictionary<string, string> _clientIdToUsername;
        private readonly Dictionary<string, MatchUnderSetup> _matches;
        private readonly Dictionary<string, string> _clientIdToMatchId;
        private readonly IGameDataService _gameData;

        public MatchSetupTracker(ILoggerProvider loggerProvider, IGameDataService gameData)
            : base(loggerProvider.GetLoggerFor(typeof(MatchSetupTracker)))
        {
            _gameData = gameData;

            _matches = new Dictionary<string, MatchUnderSetup>();
            _clientIdToMatchId = new Dictionary<string, string>();
            _clientIdToUsername = new Dictionary<string, string>();

            SetMessageHandler<StartMatchSetupRequest>(HandleStartMatchSetupRequest);
            SetMessageHandler<MatchListRequest>(HandleMatchListRequest);
            SetMessageHandler<LeaveMatchSetupMessage>(HandleLeaveMatchSetupMessage);
            SetMessageHandler<JoinMatchRequest>(HandleJoinMatchRequest);
            SetMessageHandler<SlotChangeMessage>(HandleSlotChangeMessage);
            SetMessageHandler<UserConnectedMessage>(HandleUserConnected);
            SetMessageHandler<UserDisconnectedMessage>(HandleUserDisconnected);
            SetMessageHandler<MatchSetupFinishedMessage>(HandleMatchSetupFinishedMessage);
        }
        
        private void HandleStartMatchSetupRequest(StartMatchSetupRequest message)
        {
            if (_clientIdToMatchId.ContainsKey(message.ClientId))
            {
                Logger.Warn("Received StartMatchSetupRequest for user already in a match.");
            }
            else
            {
                var match = new MatchUnderSetup(message.ClientId, _clientIdToUsername[message.ClientId] + "'s Game");
                _matches.Add(match.Id, match);
                _clientIdToMatchId.Add(message.ClientId, match.Id);
                match.Slots.First().SetPlayer(message.ClientId, _clientIdToUsername[message.ClientId]);

                SendMessage(new UserJoinedMatchMessage(message.ClientId));
                SendMessage(new MatchSetupUpdate(match.PlayersPresent, match.BuildSummary()));
            }
        }

        private void HandleMatchListRequest(MatchListRequest message)
        {
            var reply = new MatchListReply(message.ClientId)
            {
                Matches = _matches.Values
                    .Where(m => m.HasCapacity)
                    .OrderBy(m => m.Name)
                    .Select(m => m.BuildSummary())
                    .ToArray()
            };

            SendMessage(reply);
        }

        private void HandleLeaveMatchSetupMessage(LeaveMatchSetupMessage message)
        {
            if (!_clientIdToMatchId.ContainsKey(message.ClientId))
            {
                Logger.Warn("Received LeaveMatchSetupMessage for user not in a match.");
            }
            else
            {
                HandleUserLeavingMatch(message.ClientId);
            }
        }

        private void HandleJoinMatchRequest(JoinMatchRequest message)
        {
            if (_clientIdToMatchId.ContainsKey(message.ClientId))
            {
                Logger.Warn("Received JoinMatchRequest for user already not in a match.");
            }
            else if (!_matches.ContainsKey(message.MatchId))
            {
                Logger.Warn("Received JoinMatchRequest for match that does not exist.");
            }
            else
            {
                var match = _matches[message.MatchId];

                var slot = match.Slots
                    .Where(s => !s.HasPlayer)
                    .Where(s => !s.HasBot)
                    .FirstOrDefault();

                if (slot != null)
                {
                    slot.SetPlayer(message.ClientId, _clientIdToUsername[message.ClientId]);
                    SendMessage(new UserJoinedMatchMessage(message.ClientId));
                    _clientIdToMatchId.Add(message.ClientId, match.Id);
                }

                SendMessage(new MatchSetupUpdate(match.PlayersPresent, match.BuildSummary()));
            }
        }

        private void HandleSlotChangeMessage(SlotChangeMessage message)
        {
            if (!_matches.ContainsKey(message.MatchId))
            {
                Logger.Warn("Received SlotChangeMessage for match that does not exist.");
            }
            else
            {
                var match = _matches[message.MatchId];

                if(match.HostId != message.ClientId)
                {
                    Logger.Warn("Received SlotChangeMessage from user that isn't match host.");
                }
                else
                {
                    if (message.SlotIndex > 0 && message.SlotIndex < match.Slots.Length)
                    {
                        var slot = match.Slots[message.SlotIndex];

                        if (message.ClearSlot)
                        {
                            if (slot.HasPlayer)
                            {
                                SendMessage(new KickedFromMatchMessage(slot.PlayerId));
                                HandleUserLeavingMatch(slot.PlayerId);

                            }
                            else // a bot instead
                            {
                                slot.ClearSlot();
                            }
                        }

                        if (message.SetBot)
                        {
                            slot.SetBot();
                        }
                    }

                    SendMessage(new MatchSetupUpdate(match.PlayersPresent, match.BuildSummary()));
                }
            }
        }

        private void HandleUserConnected(UserConnectedMessage message)
        {
            if (_clientIdToUsername.ContainsKey(message.ClientId))
            {
                _clientIdToUsername[message.ClientId] = message.Username;
            }
            else
            {
                _clientIdToUsername.Add(message.ClientId, message.Username);
            }
        }

        private void HandleUserDisconnected(UserDisconnectedMessage message)
        {
            if (_clientIdToUsername.ContainsKey(message.ClientId))
            {
                _clientIdToUsername.Remove(message.ClientId);
            }
            
            if(_clientIdToMatchId.ContainsKey(message.ClientId))
            {
                HandleUserLeavingMatch(message.ClientId);
            }
        }

        private void HandleUserLeavingMatch(string clientId)
        {
            var match = _matches[_clientIdToMatchId[clientId]];

            // Host leaves, kick everyone out
            if (match.HostId == clientId)
            {
                SendMessage(new KickedFromMatchMessage(match.PlayersPresent));

                foreach (var playerId in match.PlayersPresent)
                {
                    _clientIdToMatchId.Remove(playerId);
                    SendMessage(new UserLeftMatchMessage(playerId));
                }

                _matches.Remove(match.Id);
            }
            else
            {
                var slot = match.Slots
                    .Where(s => s.HasPlayer)
                    .Where(s => s.PlayerId == clientId)
                    .FirstOrDefault();

                if (slot != null)
                {
                    slot.ClearSlot();
                }

                _clientIdToMatchId.Remove(clientId);

                SendMessage(new MatchSetupUpdate(match.PlayersPresent, match.BuildSummary()));
                SendMessage(new UserLeftMatchMessage(clientId));
            }
        }

        private void HandleMatchSetupFinishedMessage(MatchSetupFinishedMessage message)
        {
            if (!_matches.ContainsKey(message.MatchId))
            {
                Logger.Warn("Received SlotChangeMessage for match that does not exist.");
            }
            else
            {
                var match = _matches[message.MatchId];

                if (match.HostId != message.ClientId)
                {
                    Logger.Warn("Received SlotChangeMessage from user that isn't match host.");
                }
                else
                {
                    SendMessage(new StartMatchMessage(match));
                    SendMessage(new MatchSetupUpdate(match.PlayersPresent, match.BuildSummary()));
                }
            }
        }
    }
}
