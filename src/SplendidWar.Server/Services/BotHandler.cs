﻿using System;
using System.Collections.Generic;
using System.Threading;
using SplendidWar.Contracts.Messages;
using SplendidWar.Server.AI;
using SplendidWar.Server.Interfaces;
using SplendidWar.Server.Messages;

namespace SplendidWar.Server.Services
{
    public class BotHandler : AbstractBusService
    {
        private const int BOT_UPDATE_TICK = 1000;

        private readonly Dictionary<string, Bot> _botsByClientId;
        private readonly Dictionary<string, Timer> _botTimers;
        
        public BotHandler(ILoggerProvider loggerProvider, IGameDataService gameData)
            :base(loggerProvider.GetLoggerFor(typeof(BotHandler)))
        {
            _botsByClientId = new Dictionary<string, Bot>();
            _botTimers = new Dictionary<string, Timer>();

            SetMessageHandler<BotRequiredMessage>(HandleBotRequiredMessage);
        }
        
        public override void Stop()
        {
            base.Stop();

            foreach (var timer in _botTimers.Values)
            {
                timer.Dispose();
            }
        }

        private void HandleBotRequiredMessage(BotRequiredMessage message)
        {
            try
            {
                if (_botsByClientId.ContainsKey(message.BotId))
                {
                    Logger.Warn("Received BotRequiredMessage for bot that already exists.");
                }
                else
                {
                    var bot = new Bot(message.BotId);

                    _botsByClientId.Add(bot.ClientId, bot);
                    _botTimers.Add(bot.ClientId, new Timer(StepBot, bot, BOT_UPDATE_TICK, BOT_UPDATE_TICK));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
        
        private void StepBot(object state)
        {
            try
            {
                var bot = (Bot)state;

                // TODO handle new game state
                // TODO get orders from bot and send to game
                //bot.Step();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}
