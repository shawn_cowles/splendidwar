﻿using System;
using System.Collections.Generic;
using System.Linq;
using SplendidWar.Contracts.Messages;
using SplendidWar.Server.Data;
using SplendidWar.Server.Interfaces;
using SplendidWar.Server.Messages;

namespace SplendidWar.Server.Services
{
    public class UserTracker : AbstractBusService
    {
        private readonly Dictionary<string, UserSession> _sessionsByClientId;

        public UserTracker(ILoggerProvider loggerProvider)
            : base(loggerProvider.GetLoggerFor(typeof(UserTracker)))
        {
            _sessionsByClientId = new Dictionary<string, UserSession>();

            SetMessageHandler<UserConnectedMessage>(HandleUserConnected);
            SetMessageHandler<UserDisconnectedMessage>(HandleUserDisconnected);
            SetMessageHandler<UserListRequest>(HandleUserListRequest);
            SetMessageHandler<UserJoinedMatchMessage>(HandleUserJoinedMatchMessage);
            SetMessageHandler<UserLeftMatchMessage>(HandleUserLeftMatchMessage);
        }
        
        private void HandleUserConnected(UserConnectedMessage message)
        {
            if (_sessionsByClientId.ContainsKey(message.ClientId))
            {
                Logger.Warn("Received UserConnectedMessage for already connected user.");
            }
            else
            {
                var session = new UserSession
                {
                    UserId = message.UserId,
                    StartDate = DateTime.UtcNow,
                    Username = message.Username
                };

                _sessionsByClientId.Add(message.ClientId, session);

                SendMessage(new UserConnectionReply(message.ClientId));
            }
        }

        private void HandleUserDisconnected(UserDisconnectedMessage message)
        {
            if (!_sessionsByClientId.ContainsKey(message.ClientId))
            {
                Logger.Warn("Received UserDisconnectedMessage for user without active session.");
            }
            else
            {
                var session = _sessionsByClientId[message.ClientId];
                session.EndDate = DateTime.UtcNow;
                session.InMatch = false;

                _sessionsByClientId.Remove(message.ClientId);

                SendMessage(new SessionEndedMessage(session));
            }
        }

        private void HandleUserListRequest(UserListRequest message)
        {
            var reply = new UserListMessage(message.ClientId);

            reply.Usernames = _sessionsByClientId.Values
                .Where(s => !s.InMatch)
                .OrderBy(s => s.StartDate)
                .Select(s => s.Username)
                .ToArray();

            reply.Count = reply.Usernames.Length;

            SendMessage(reply);
        }

        private void HandleUserJoinedMatchMessage(UserJoinedMatchMessage message)
        {
            if (!_sessionsByClientId.ContainsKey(message.ClientId))
            {
                Logger.Warn("Received UserJoinedMatchMessage for user without active session.");
            }
            else
            {
                _sessionsByClientId[message.ClientId].InMatch = true;
            }
        }

        private void HandleUserLeftMatchMessage(UserLeftMatchMessage message)
        {
            if (!_sessionsByClientId.ContainsKey(message.ClientId))
            {
                Logger.Warn("Received UserLeftMatchMessage for user without active session.");
            }
            else
            {
                _sessionsByClientId[message.ClientId].InMatch = false;
            }
        }
    }
}
