﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ExtensibleBinaryTag;
using SplendidWar.Common.Data;
using SplendidWar.Server.Interfaces;

namespace SplendidWar.Server.Services
{
    public class GameDataService : IGameDataService, IInitializableService
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly ILogger _logger;

        private Dictionary<string, ShipData> _shipDataByName;

        public GameDataService(ISettingsProvider settingsProvider, ILoggerProvider loggerProvider)
        {
            _settingsProvider = settingsProvider;
            _logger = loggerProvider.GetLoggerFor(typeof(GameDataService));
        }

        public void Initialize()
        {
            try
            {
                var path = Path.Combine(_settingsProvider.GetDataDirectory, "data.xbt");

                XBTag root;

                using (var stream = new FileStream(path, FileMode.Open))
                {
                    root = XBTag.ReadFromStream(stream);
                }

                var ships = XBTag.ReadCollection(root.GetChild("ships"), ShipData.FromTag);

                _shipDataByName = ships.ToDictionary(s => s.Name);
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public MapData GetTestMap()
        {
            return new MapData()
            {
                Width = 10000,
                Height = 10000
            };
        }
        
        public ShipData GetDataForShip(string shipName)
        {
            if(_shipDataByName.ContainsKey(shipName))
            {
                return _shipDataByName[shipName];
            }

            throw new Exception("No data found for ship with class name: " + shipName);
        }
    }
}
