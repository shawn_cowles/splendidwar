﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using SplendidWar.Server.Interfaces;
using SplendidWar.Server.Services;

namespace SplendidWar.Server
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var container = BuildIoCContainer();

            var logger = container.Resolve<ILoggerProvider>().GetLoggerFor(typeof(Program));

            try
            {
                logger.Info("Beginning Server Startup");

                foreach (var initializable in container.Resolve<IEnumerable<IInitializableService>>())
                {
                    initializable.Initialize();
                }

#if DEBUG
                var service = container.Resolve<Service>();

                service.DebugStart();


                logger.Info("SplendidWar Server Started");

                while (Console.ReadKey().KeyChar != 'q')
                {
                    Console.WriteLine();
                    continue;
                }

                service.DebugStop();
#else
                System.ServiceProcess.ServiceBase.Run(container.Resolve<Service>());
#endif
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        private static IContainer BuildIoCContainer()
        {
            var builder = new ContainerBuilder();

            // Supporting Services
            builder.RegisterType<AppConfigSettingsProvider>()
                .AsImplementedInterfaces()
                .SingleInstance();
            builder.RegisterType<Log4NetLoggerProvider>()
                .AsImplementedInterfaces()
                .SingleInstance();
            builder.RegisterType<GameDataService>()
                .AsImplementedInterfaces()
                .SingleInstance();

            // Bus Services
            var busServices = typeof(Program)
                .Assembly
                .GetTypes()
                .Where(t => t.IsSubclassOf(typeof(AbstractBusService)))
                .ToArray();
            
            foreach(var type in busServices)
            {
                builder.RegisterType(type)
                    .AsImplementedInterfaces()
                    .SingleInstance();
            }
            
            //The Service Itself
            builder.RegisterType<Service>().As<Service>();

            return builder.Build();
        }
    }
}
