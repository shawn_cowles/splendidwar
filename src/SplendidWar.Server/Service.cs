﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using SplendidWar.Server.Interfaces;

namespace SplendidWar.Server
{
    public partial class Service : ServiceBase
    {
        private readonly ILogger _logger;
        private readonly IBusService[] _busServices;

        private MessageBus _messageBus;

        public Service(ILoggerProvider loggerProvider, IEnumerable<IBusService> busServices)
        {
            _logger = loggerProvider.GetLoggerFor(typeof(Service));
            _busServices = busServices.ToArray();
            _messageBus = new MessageBus(_busServices);

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                base.OnStart(args);

                _logger.Info("SSG Service Starting");

                foreach (var service in _busServices)
                {
                    service.Start(_messageBus);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        protected override void OnStop()
        {
            try
            {
                base.OnStop();

                _logger.Info("SSG Service Stopping");

                foreach (var service in _busServices)
                {
                    service.Stop();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public void DebugStart()
        {
            OnStart(new string[0]);
        }
        
        public void DebugStop()
        {
            OnStop();
        }
    }
}
