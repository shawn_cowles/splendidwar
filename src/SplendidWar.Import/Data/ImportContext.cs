﻿using System.Collections.Generic;
using System.IO;
using SplendidWar.Common.Data;

namespace SplendidWar.Import.Data
{
    public class ImportContext
    {
        public string ImportPath { get; private set; }

        public string MapPath
        {
            get { return Path.Combine(ImportPath, "Maps"); }
        }

        public string ShipPath
        {
            get { return Path.Combine(ImportPath, "Ships"); }
        }
        
        public string ServerExportPath { get; private set; }

        public string ClientExportPath { get; private set; }

        //public Dictionary<string, Zone> Zones { get; private set; }

        //public Dictionary<string, TmxMap> ZoneMaps { get; private set; }

        public List<ShipData> Ships { get; private set; }

        public ImportContext(string importPath, string serverExportPath, string clientExportPath)
        {
            ImportPath = importPath;
            ServerExportPath = serverExportPath;
            ClientExportPath = clientExportPath;
            //Zones = new Dictionary<string, Zone>();
            //ZoneMaps = new Dictionary<string, TmxMap>();
            Ships = new List<ShipData>();
        }
    }
}
