﻿namespace SplendidWar.Import.Data
{
    public static class ObjectTypes
    {
        public const string SHIP_ENGINE = "engine";
        public const string SHIP_BARREL = "barrel";
        public const string SHIP_WEAPON = "weapon";
    }
}
