﻿namespace SplendidWar.Import.Data
{
    public static class PropertyNames
    {
        public const string SHIP_ARMOR = "armor";
        public const string SHIP_HEALTH = "health";
        public const string SHIP_EVASION = "evasion";
        public const string SHIP_SPEED = "speed";
        public const string SHIP_ACCELERATION = "acceleration";
        public const string SHIP_TURN_RATE = "turn_rate";

        public const string ENGINE_WIDTH = "width";

        public const string BARREL_FACING = "facing";
        public const string BARREL_PARENT_WEAPON = "parent_weapon";
        
        public const string WEAPON_TYPE = "weapon_type";
        public const string WEAPON_MOUNTING = "fixed_mounting";
        public const string WEAPON_DAMAGE = "damage";
        public const string WEAPON_SHOTS = "shots";
        public const string WEAPON_ARMOR_PENETRATION = "armor_penetration";
        public const string WEAPON_ACCURACY = "accuracy";
        public const string WEAPON_COOLDOWN = "cooldown";
        public const string WEAPON_RANGE = "range";
    }
}
