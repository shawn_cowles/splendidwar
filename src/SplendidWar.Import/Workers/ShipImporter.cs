﻿using System;
using System.IO;
using System.Linq;
using SplendidWar.Common.Data;
using SplendidWar.Import.Data;
using TiledSharp;

namespace SplendidWar.Import.Workers
{
    public class ShipImporter : AbstractWorker
    {
        public override string Name
        {
            get { return "Ship Importer"; }
        }

        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                var shipFiles = Directory.EnumerateFiles(importContext.ShipPath)
                    .Where(f => Path.GetExtension(f) == ".tmx")
                    .ToArray();

                foreach (var shipFile in shipFiles)
                {
                    try
                    {
                        var map = new TmxMap(shipFile);

                        importContext.Ships.Add(ImportShipFromMap(
                            Path.GetFileNameWithoutExtension(shipFile),
                            map));
                    }
                    catch (Exception ex)
                    {
                        throw new ImportException($"Failed to import ship '{shipFile}'", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }

            return new WorkResult(Name, true, string.Empty);
        }

        private ShipData ImportShipFromMap(string className, TmxMap map)
        {
            var shipData = new ShipData();

            shipData.Name = className;
            shipData.Armor = GetIntProperty(map, PropertyNames.SHIP_ARMOR);
            shipData.Health = GetIntProperty(map, PropertyNames.SHIP_HEALTH);
            shipData.Evasion = GetDoubleProperty(map, PropertyNames.SHIP_EVASION);
            shipData.Speed = GetDoubleProperty(map, PropertyNames.SHIP_SPEED);
            shipData.Acceleration = GetDoubleProperty(map, PropertyNames.SHIP_ACCELERATION);
            shipData.TurnRate = GetDoubleProperty(map, PropertyNames.SHIP_TURN_RATE);
            shipData.BaseSprite = className;
            
            foreach (var layer in map.ObjectGroups)
            {
                foreach (var obj in layer.Objects)
                {
                    try
                    {
                        switch (obj.Type)
                        {
                            case ObjectTypes.SHIP_ENGINE:
                                ImportEngine(map, shipData, obj);
                                break;
                            case ObjectTypes.SHIP_BARREL:
                                ImportBarrel(map, obj);
                                break;
                            case ObjectTypes.SHIP_WEAPON:
                                ImportWeapon(map, shipData, obj);
                                break;
                            //default:
                                //throw new ImportException($"Unknown object type '{obj.Type}'");
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ImportException($"Failed to import object '{obj.Name}' in layer '{layer.Name}'", ex);
                    }
                }
            }
            
            return shipData;
        }
        
        private void ImportEngine(TmxMap map, ShipData ship, TmxObject engine)
        {
            ship.Engines.Add(new Engine
            {
                Offset = new Vector2(
                    engine.X - (map.Width * map.TileWidth) / 2,
                    engine.Y - (map.Height * map.TileHeight) / 2),

                Width = GetDoubleProperty(engine, PropertyNames.ENGINE_WIDTH)
            });
        }
        
        private void ImportBarrel(TmxMap map, TmxObject barrel)
        {
            // just validation, import handled with the weapon
            var weaponName = GetProperty(barrel, PropertyNames.BARREL_PARENT_WEAPON);

            var parent = map.ObjectGroups
                .SelectMany(g => g.Objects)
                .Where(o => o.Type == ObjectTypes.SHIP_WEAPON)
                .Where(o => o.Name == weaponName)
                .FirstOrDefault();

            if(parent == null)
            {
                throw new ImportException($"Object '{barrel.Name}' references a parent weapon '{weaponName}' that does not exist.");
            }
        }

        private void ImportWeapon(TmxMap map, ShipData ship, TmxObject weapon)
        {
            var barrels = map.ObjectGroups
                .SelectMany(g => g.Objects)
                .Where(o => o.Type == ObjectTypes.SHIP_BARREL)
                .Where(o => GetProperty(o, PropertyNames.BARREL_PARENT_WEAPON) == weapon.Name)
                .Select(o=> new BarrelOffset
                {
                    Facing = GetDoubleProperty(o, PropertyNames.BARREL_FACING),
                    Offset = new Vector2(
                        o.X - (map.Width * map.TileWidth) / 2,
                        o.Y - (map.Height * map.TileHeight) / 2),
                })
                .ToArray();

            if(!barrels.Any())
            {
                throw new ImportException($"Weapon '{weapon.Name}' does not have any barrels.");
            }

            var weaponSystem = new WeaponSystem
            {
                BarrelOffsets = barrels,

                Type = GetProperty(weapon, PropertyNames.WEAPON_TYPE),
                FixedMounting = GetBoolProperty(weapon, PropertyNames.WEAPON_MOUNTING),
                Shots = GetIntProperty(weapon, PropertyNames.WEAPON_SHOTS),
                ArmorPenetration = GetDoubleProperty(weapon, PropertyNames.WEAPON_ARMOR_PENETRATION),
                Accuracy = GetDoubleProperty(weapon, PropertyNames.WEAPON_ACCURACY),
                Cooldown = GetDoubleProperty(weapon, PropertyNames.WEAPON_COOLDOWN),
                Range = GetDoubleProperty(weapon, PropertyNames.WEAPON_RANGE),
            };

            ship.Weapons.Add(weaponSystem);
        }
    }
}
