﻿using System;
using System.IO;
using ExtensibleBinaryTag;
using SplendidWar.Import.Data;

namespace SplendidWar.Import.Workers
{
    public class ServerExporter : AbstractWorker
    {
        public override string Name
        {
            get { return "Server Exporter"; }
        }

        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                var root = new XBTag("data");
                root.AddChild(XBTag.BuildCollection("ships", importContext.Ships));

                var dataOutPath = Path.Combine(
                    importContext.ServerExportPath,
                    "data.xbt");

                using (var stream = new FileStream(dataOutPath, FileMode.Create))
                {
                    XBTag.WriteToStream(root, stream);
                }
            }
            catch(Exception ex)
            {
                return ReportOnException(ex);
            }

            return new WorkResult(Name, true, "");
        }
    }
}
