﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using SplendidWar.Contracts.Data;
using SplendidWar.Import.Data;

namespace SplendidWar.Import.Workers
{
    public class ClientExporter : AbstractWorker
    {
        public override string Name
        {
            get { return "Client Exporter"; }
        }

        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                var shipSpriteNames = importContext.Ships
                    .Select(s => s.BaseSprite)
                    .ToArray();

                var shipImagefiles = shipSpriteNames
                    .Select(s => s + ".png")
                    .ToArray();

                CopyImages(shipImagefiles, 
                    Path.Combine(importContext.ImportPath, "Ships"),
                    Path.Combine(importContext.ClientExportPath, "images", "ships"));

                var manifest = new ClientManifest
                {
                    ShipImages = shipSpriteNames
                };

                WriteManifest(
                    Path.Combine(importContext.ClientExportPath, "data", "manifest.json"),
                    manifest);
            }
            catch(Exception ex)
            {
                return ReportOnException(ex);
            }

            return new WorkResult(Name, true, "");
        }
        
        private void CopyImages(string[] imageFiles, string sourceDirectory, string destinationDirectory)
        {
            foreach(var file in imageFiles)
            {
                var sourceName = Path.Combine(sourceDirectory, file);
                var destinationName = Path.Combine(destinationDirectory, file);

                if(!File.Exists(sourceName))
                {
                    throw new Exception("Expected image file does not exist: " + sourceName);
                }

                File.Copy(sourceName, destinationName, true);
            }
        }

        private void WriteManifest(string path, ClientManifest manifest)
        {
            using (var stream = new FileStream(path, FileMode.Create))
            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine(JsonConvert.SerializeObject(manifest));
            }
        }
    }
}
