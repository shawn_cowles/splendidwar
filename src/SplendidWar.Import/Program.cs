﻿using System;
using System.IO;
using System.Windows.Forms;

namespace SplendidWar.Import
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            var importPath = "";
            var serverExportPath = "";
            var clientExportPath = "";

            if (args.Length >= 3)
            {
                importPath = Directory.GetParent(Path.GetDirectoryName(args[0])).FullName;
                serverExportPath = args[1];
                clientExportPath = args[2];
            }


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ImportResultsForm(importPath, serverExportPath, clientExportPath));
        }
    }
}
